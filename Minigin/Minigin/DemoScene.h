#pragma once
#include "Scene.h"
#include <string>
#include "TextComponent.h"

namespace dae
{
	class DemoScene : public Scene
	{
	public:
		DemoScene(const std::string& name);
		void Init() override;
		void Update() override;

	private:
		TextComponent* m_pTextComp;
		GameObject* Logo;
	};
}