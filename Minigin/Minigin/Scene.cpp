#include "MiniginPCH.h"
#include "Scene.h"
#include "GameObject.h"
#include <algorithm>
#include "Transform.h"
#include <thread>
#include "DataVault.h"
#include "Renderer.h"
#include "CollisionComponent.h"
//#include "ComponentManager.h"
//#include "InputManager.h"

unsigned int dae::Scene::idCounter = 0;

//#define DebugRender
//#define DebugCollisionGrid

dae::Scene::Scene(const std::string& name) : m_Name(name) {}

dae::Scene::~Scene()
{
	delete m_pManagers->pComp;
	delete m_pManagers->pInput;
	delete m_pManagers->pCollision;
	delete m_pManagers;
	for (auto gameObject : m_Objects)
	{
		delete gameObject;
	}
	for (auto gameObject : m_ThreadedObjects)
	{
		delete gameObject;
	}
}

void dae::Scene::Init()
{
	m_pManagers = new SceneManagers;
	m_pManagers->pComp = new ComponentManager();
	m_pManagers->pInput = new InputManager();
	m_pManagers->pCollision = new CollisionManager();
}

void dae::Scene::Add(GameObject* object)
{
	CollisionComponent* pCollider = object->GetComponent<CollisionComponent>();
	if (pCollider) pCollider->Add();
	if (object->IsThreaded())
	{
		m_ThreadedObjects.push_back(object);
	}
	else
	{
		m_Objects.push_back(object);
	}	
	m_pManagers->pComp->SortRenderComponents();
	object->SetParent(this);
}

bool dae::Scene::HandleInput()
{
	return m_pManagers->pInput->ProcessInput();
}

void dae::Scene::Update()
{
	std::vector<std::thread> threads{};
	threads.reserve(m_ThreadedObjects.size());
	for (unsigned int i = 0; i < m_ThreadedObjects.size(); ++i)
	{
		if (m_ThreadedObjects[i]->IsActive())
			threads.push_back(std::thread(&GameObject::Update, m_ThreadedObjects[i]));
	}
	for(auto gameObject : m_Objects)
	{
		if (gameObject->IsActive())
		gameObject->Update();
	}
	for (unsigned int i = 0; i < threads.size(); ++i)
	{
		threads[i].join();
	}
}

void dae::Scene::Render()
{
	for (Component* pComp : m_pManagers->pComp->GetRenderComponents())
	{
		if (pComp->GetGameObject()->GetTag() == "Pacman")
		{
			int i = 5;
			++i;
		}
		if (pComp->GetGameObject()->IsActive()) pComp->Render();
	}

#ifdef DebugRender
	for (Component* pComp : m_pManagers->pComp->GetComponents())
	{
		pComp->Render();
	}
#endif
#ifdef DebugCollisionGrid
	for (int i = 0 ; i < DataVault::GetInstance().windowHeight; i += DataVault::GetInstance().windowHeight / 10)
	{
		SDL_RenderDrawLine(Renderer::GetInstance().GetSDLRenderer(), 0, i, DataVault::GetInstance().windowWidth, i);
	}
	for (int i = 0; i < DataVault::GetInstance().windowWidth; i += DataVault::GetInstance().windowWidth / 10)
	{
		SDL_RenderDrawLine(Renderer::GetInstance().GetSDLRenderer(), i, 0, i, DataVault::GetInstance().windowHeight);
	}
#endif


	/*
	std::sort(m_Objects.begin(), m_Objects.end(), [](GameObject* A, GameObject* B) { return A->GetTransform()->GetPosition().z < B->GetTransform()->GetPosition().z; });
	for (const auto gameObject : m_Objects)
	{
		gameObject->Render();
	}
	*/
}

