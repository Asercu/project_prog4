#pragma once
#include "Component.h"
#include <string>
#include <glm/detail/type_vec.hpp>

namespace dae
{
	class ComponentManager;

	class TextureComponent : public Component
	{
	public:
		void Render() override;
		void SetPivot(glm::vec3* pivot);
		void SetTexture(std::string file);
		Texture2D* GetTexture() { return m_pTex; };
		glm::vec3* GetPivot() { return m_pPivot; };

		TextureComponent(ComponentManager* pCompManager);

	private:
		Texture2D* m_pTex;
		glm::vec3* m_pPivot;
	};
}
