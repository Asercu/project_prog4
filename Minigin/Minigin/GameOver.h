#pragma once
#include "Scene.h"

namespace dae
{
	class TextComponent;

	class GameOverScene : public Scene
	{
	public:
		GameOverScene(const std::string& SceneName);
		void Init() override;
		void Update() override;

		void Previous();
		void Next();
		void Select();

	private:
		int selectedIndex = 0;
		TextComponent* m_pRetry;
		TextComponent* m_pMainMenu;
		TextComponent* m_pExit;

		void SetColors();
	};
}