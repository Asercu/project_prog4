#include "MiniginPCH.h"
#include "Minigin.h"
#include <chrono>
#include <thread>
#include "InputManager.h"
#include "SceneManager.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include <SDL.h>
#include "GameObject.h"
#include "Scene.h"
#include "Time.h"
#include "DemoScene.h"
#include "DataVault.h"
#include "PacmanScene.h"
#include "MainMenuScene.h"
#include "PauseScene.h"
#include "GameOver.h"


void dae::Minigin::Initialize()
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0) 
	{
		throw std::runtime_error(std::string("SDL_Init Error: ") + SDL_GetError());
	}

	window = SDL_CreateWindow(
		"Programming 4 assignment",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		640,
		480,
		SDL_WINDOW_OPENGL
	);

	DataVault::GetInstance().windowHeight = 480;
	DataVault::GetInstance().windowWidth = 640;

	if (window == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateWindow Error: ") + SDL_GetError());
	}

	Renderer::GetInstance().Init(window);
}

/**
 * Code constructing the scene world starts here
 */
void dae::Minigin::LoadGame() const
{
	std::shared_ptr<Scene> mainMenu(new MainmenuScene("main"));
	SceneManager::GetInstance().AddScene(mainMenu);
	std::shared_ptr<Scene> pauseMenu(new PauseScene("pause"));
	SceneManager::GetInstance().AddScene(pauseMenu);
	std::shared_ptr<Scene> gameOver(new GameOverScene("gameOver"));
	SceneManager::GetInstance().AddScene(gameOver);
	SceneManager::GetInstance().SetActiveScene("main");
}

void dae::Minigin::Cleanup()
{
	Renderer::GetInstance().Destroy();
	ResourceManager::GetInstance().Destroy();
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();
}

void dae::Minigin::Run()
{
	Initialize();

	// tell the resource manager where he can find the game data
	ResourceManager::GetInstance().Init("../Data/");

	LoadGame();
	{
		auto& time = dae::Time::GetInstance();
		time.Initialize();
		//time.SetEnableFixedTime(true);
		auto& renderer = Renderer::GetInstance();
		auto& sceneManager = SceneManager::GetInstance();
		//auto& input = InputManager::GetInstance();

		bool doContinue = true;
		while (doContinue)
		{
			time.Update();
			doContinue = sceneManager.HandleInput();// input.ProcessInput();
			sceneManager.Update();
			renderer.Render();
		}
	}

	Cleanup();
}
