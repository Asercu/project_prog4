#pragma once
#include "Singleton.h"


namespace dae
{
	class DataVault : public Singleton<DataVault>
	{
	public:
		int windowHeight;
		int windowWidth;
		std::string pausedScene;
		int scoreP1 = 4567;
		int scoreP2 = 1234;
	};
}
