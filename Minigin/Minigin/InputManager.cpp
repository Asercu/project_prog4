#include "MiniginPCH.h"
#include "InputManager.h"
#include <SDL.h>
#include "Structs.h"
#include <thread>

bool dae::InputManager::ProcessInput()
{
	m_LastControllerStates = m_CurrentControllerStates;

	for (int i = 0; i < 4; ++i)
	{
		ZeroMemory(&m_CurrentControllerStates[i], sizeof(XINPUT_STATE));
		XInputGetState(i, &m_CurrentControllerStates[i]);
	}

	std::vector<std::thread> threads{};
	threads.reserve(m_ThreadedActions.size());
	for (unsigned int i = 0 ; i < m_ThreadedActions.size() ; ++i)
	{
		if (m_ThreadedActions[i]->IsActive)
			threads.push_back(std::thread(&CommandInterface::Execute, m_ThreadedActions[i]));
	}

	for (CommandInterface* cmd : m_Actions)
	{
		if (cmd->IsActive)
		HandleCmd(cmd);
	}

	for (unsigned int i = 0; i < threads.size(); ++i)
	{
		threads[i].join();
	}

	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			return false;
		}
		if (e.type == SDL_KEYDOWN) {

		}
		if (e.type == SDL_MOUSEBUTTONDOWN) {

		}
	}

	return true;
}

bool dae::InputManager::IsDown(XINPUT_STATE state, ControllerButton button) const
{
	switch (button)
	{
	case ControllerButton::ButtonA:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_A;
		break;
	case ControllerButton::ButtonB:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_B;
		break;
	case ControllerButton::ButtonX:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_X;
		break;
	case ControllerButton::ButtonY:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_Y;
		break;
	case ControllerButton::ButtonLB:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER;
		break;
	case ControllerButton::ButtonRB:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER;
		break;
	case ControllerButton::ButtonBACK:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK;
		break;
	case ControllerButton::ButtonSTART:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_START;
		break;
	case ControllerButton::ButtonLSTICK:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB;
		break;
	case ControllerButton::ButtonRSTICK:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB;
		break;
	case ControllerButton::DPadUP:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP;
		break;
	case ControllerButton::DPadDown:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN;
		break;
	case ControllerButton::DPadRight:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT;
		break;
	case ControllerButton::DPadLeft:
		return state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT;
		break;
	case ControllerButton::TriggerLeft:
		return state.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
		break;
	case ControllerButton::TriggerRight:
		return state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
		break;
	default: return false;
	}
}

void dae::InputManager::AddAction(CommandInterface* cmd)
{
	if (cmd->Threaded)
	{
		m_ThreadedActions.push_back(cmd);
	}
	else
	{
		m_Actions.push_back(cmd);
	}
}

dae::float2 dae::InputManager::GetLeftStickPos(unsigned controllerID)
{
	float x = m_CurrentControllerStates[controllerID].Gamepad.sThumbLX;
	float y = m_CurrentControllerStates[controllerID].Gamepad.sThumbLY;
	if (x < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && x > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) x = 0.f;
	if (y < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && y > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) y = 0.f;	
	
	if (x < 0)
	{
		x /= -SHRT_MIN;
	}
	else
	{
		x /= SHRT_MAX;
	}

	if (y < 0)
	{
		y /= -SHRT_MIN;
	}
	else
	{
		y /= SHRT_MAX;
	}

	return float2(x, y);
}

dae::float2 dae::InputManager::GetRightStickPos(unsigned controllerID)
{
	float x = m_CurrentControllerStates[controllerID].Gamepad.sThumbRX;
	float y = m_CurrentControllerStates[controllerID].Gamepad.sThumbRY;
	if (x < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && x > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) x = 0.f;
	if (y < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && y > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) y = 0.f;

	if (x < 0)
	{
		x /= -SHRT_MIN;
	}
	else
	{
		x /= SHRT_MAX;
	}

	if (y < 0)
	{
		y /= -SHRT_MIN;
	}
	else
	{
		y /= SHRT_MAX;
	}

	return float2(x, y);
}

float dae::InputManager::GetLeftTrigger(unsigned controllerID)
{
	return m_CurrentControllerStates[controllerID].Gamepad.bLeftTrigger / (float)UCHAR_MAX;
}

float dae::InputManager::GetRightTrigger(unsigned controllerID)
{
	return m_CurrentControllerStates[controllerID].Gamepad.bRightTrigger / (float)UCHAR_MAX;
}

dae::InputManager::InputManager()
{
	for (int i = 0; i < 4; ++i)
	{
		m_CurrentControllerStates.push_back({});
		m_LastControllerStates.push_back({});
	}
}

dae::InputManager::~InputManager()
{
	for (CommandInterface* cmd : m_Actions)
	{
		delete cmd;
	}
	for (CommandInterface* cmd : m_ThreadedActions)
	{
		delete cmd;
	}
}

void dae::InputManager::HandleCmd(CommandInterface* cmd)
{
	switch (cmd->device)
	{
	case InputDevice::Controller:
	{
		switch (cmd->action)
		{
		case InputAction::Pressed:
			if (IsDown(m_CurrentControllerStates[cmd->controllerID], cmd->btn) && !IsDown(m_LastControllerStates[cmd->controllerID], cmd->btn))
			{
				cmd->Execute();
			}
			break;
		case InputAction::Down:
			if (IsDown(m_CurrentControllerStates[cmd->controllerID], cmd->btn))
			{
				cmd->Execute();
			}
			break;
		case InputAction::Released:
			if (!IsDown(m_CurrentControllerStates[cmd->controllerID], cmd->btn) && IsDown(m_LastControllerStates[cmd->controllerID], cmd->btn))
			{
				cmd->Execute();
			}
			break;
		}
	}
	break;
	case InputDevice::KeyBoard:
	{
		switch (cmd->action)
		{
		case InputAction::Down:
			if (m_KeyboardState[cmd->key])
			{
				cmd->Execute();
			}
			break;
		case InputAction::Pressed:
			if (m_KeyboardState[cmd->key])
			{
				if (!cmd->executed)
				{
					cmd->Execute();
					cmd->executed = true;
				}
			}
			else
			{
				cmd->executed = false;
			}
			break;
		case InputAction::Released:
			if (m_KeyboardState[cmd->key])
			{
				cmd->executed = false;
			}
			else
			{
				if (!cmd->executed)
				{
					cmd->Execute();
					cmd->executed = true;
				}
			}
			break;
		}
	}
	break;
	case InputDevice::AutoExecute:
	{
		cmd->Execute();
	}
	break;
	}
}
