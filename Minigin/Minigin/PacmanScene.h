#pragma once
#include "Scene.h"

namespace dae
{
	class Pacman;
	class TextComponent;
	class Ghost;

	class PacmanScene : public Scene
	{
	public:
		PacmanScene(const std::string& levelName);
		void Init() override;
		void Update() override;

		void AddScore(int points, GameObject* p);
		void PelletCollected(GameObject* pObj);
		void MegaPelletCollected();
		void KillPacman(GameObject* pacman);
		void Pause();

	private:
		void LoadLevelFromFile();
		std::string m_levelName;
		Pacman* m_Player1;
		GameObject* m_Player2;
		Ghost* m_Red;
		Ghost* m_Orange;
		Ghost* m_Blue;
		Ghost* m_Pink;
		int m_PelletCount = 0;
		int m_ScoreP1 = 0;
		int m_ScoreP2 = 0;
		TextComponent* m_pScoreTextP1;
		TextComponent* m_pScoreTextP2;
		int m_LivesP1 = 3;
		GameObject* m_LiveObjectsP1[3];
		int m_LivesP2 = 3;
		GameObject* m_LiveObjectsP2[3];
		float2 m_Player1Start;
		float2 m_Player2Start;
	};
}
