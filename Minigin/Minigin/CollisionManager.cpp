#include "MiniginPCH.h"
#include  "CollisionManager.h"
#include "DataVault.h"
#include "CollisionComponent.h"
#include <algorithm>

dae::CollisionManager::CollisionManager()
{
	m_WindowHeight = DataVault::GetInstance().windowHeight;
	m_WindowWidth = DataVault::GetInstance().windowWidth;

	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			m_pColliders[i][j] = new std::vector<CollisionComponent*>();
		}
	}
	m_pDynamicColliders = new std::vector<CollisionComponent*>();
}

dae::CollisionManager::~CollisionManager()
{
	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			delete m_pColliders[i][j];
		}
	}
	delete m_pDynamicColliders;
}

void dae::CollisionManager::AddCollider(CollisionComponent* c)
{
	if (c->GetDynamic())
	{
		m_pDynamicColliders->push_back(c);
		return;
	}

	int left = c->box.x;
	int top = c->box.y;
	int width = c->box.w;
	int height = c->box.h;

	int minColumn = left / (m_WindowWidth / 10);
	int maxColumn = (left + width) / (m_WindowWidth / 10);
	if (maxColumn > 9) maxColumn = 9;
	if (minColumn < 0) minColumn = 0;
	int minRow = top / (m_WindowHeight / 10);
	int maxRow = (top + height) / (m_WindowHeight / 10);
	if (minRow < 0) minRow = 0;
	if (maxRow > 9) maxRow = 9;

	for (int row = minRow; row <= maxRow; row++)
	{
		for (int column = minColumn; column <= maxColumn; column++)
		{
			m_pColliders[column][row]->push_back(c);
		}
	}
}

std::vector<dae::GameObject*>* dae::CollisionManager::GetOverLaps(CollisionComponent* c)
{
	std::vector<GameObject*>* returnValue = new std::vector<GameObject*>();

	int left = c->box.x;
	int top = c->box.y;
	int width = c->box.w;
	int height = c->box.h;

	int minColumn = left / (m_WindowWidth / 10);
	int maxColumn = (left + width) / (m_WindowWidth / 10);
	if (maxColumn > 9) maxColumn = 9;
	if (minColumn < 0) minColumn = 0;
	int minRow = top / (m_WindowHeight / 10);
	int maxRow = (top + height) / (m_WindowHeight / 10);
	if (minRow < 0) minRow = 0;
	if (maxRow > 9) maxRow = 9;

	for (int row = minRow; row <= maxRow; row++)
	{
		for (int column = minColumn; column <= maxColumn; column++)
		{
			for (CollisionComponent* loopC : *m_pColliders[column][row])
			{
				//if (std::find(*returnValue->begin(), *returnValue->end(), loopC->GetGameObject()) == *returnValue->end())
				{
					if (IsOverlapping(loopC, c)) returnValue->push_back(loopC->GetGameObject());
				}
			}

		}
	}
	for (CollisionComponent* loopC : *m_pDynamicColliders)
	{
		//if (std::find(*returnValue->begin(), *returnValue->end(), loopC->GetGameObject()) == *returnValue->end())
		{
			if (IsOverlapping(loopC, c)) returnValue->push_back(loopC->GetGameObject());
		}
	}

	return returnValue;
}

bool dae::CollisionManager::RayCast(float2 begin, float2 end, HitInfo& hit, CollisionComponent* target)
{
	std::vector<HitInfo> hits;

	RaycastLineCheck(hits, begin, end, { target->box.x, target->box.y }, { target->box.x + target->box.w, target->box.y }, target);
	RaycastLineCheck(hits, begin, end, { target->box.x + target->box.w, target->box.y }, { target->box.x + target->box.w, target->box.y + target->box.h }, target);
	RaycastLineCheck(hits, begin, end, { target->box.x + target->box.w, target->box.y + target->box.h }, { target->box.x, target->box.y + target->box.h }, target);
	RaycastLineCheck(hits, begin, end, { target->box.x, target->box.y + target->box.h }, { target->box.x, target->box.y }, target);

	if (hits.size() == 0)
	{
		return false;
	}

	// Get closest intersection point and copy it into the hitInfo parameter
	hit = *std::min_element(hits.begin(), hits.end(),
		[](const HitInfo& first, const HitInfo& last) {
		return first.lambda < last.lambda; });
	return true;
}

bool dae::CollisionManager::RayCast(float2 begin, float2 end, HitInfo & hit, ColliderTypes toHit, CollisionComponent* ignore, std::string ignoreTag)
{
	std::vector<HitInfo> hits;

	SDL_Rect A;
	A.x = int(begin.x < end.x ? begin.x : end.x);
	A.y = int(begin.y < end.y ? begin.y : end.y);
	A.w = int(begin.x < end.x ? end.x - begin.x : begin.x - end.x);
	A.h = int(begin.y < end.y ? end.y - begin.y : begin.y - end.y);

	int left = A.x;
	int top = A.y;
	int width = A.w;
	int height = A.h;

	int minColumn = left / (m_WindowWidth / 10);
	int maxColumn = (left + width) / (m_WindowWidth / 10);
	if (maxColumn > 9) maxColumn = 9;
	if (minColumn < 0) minColumn = 0;
	int minRow = top / (m_WindowHeight / 10);
	int maxRow = (top + height) / (m_WindowHeight / 10);
	if(minRow < 0) minRow = 0;
	if (maxRow > 9) maxRow = 9;
	for (int row = minRow; row <= maxRow; row++)
	{
		for (int column = minColumn; column <= maxColumn; column++)
		{
			for (CollisionComponent* c : *m_pColliders[column][row])
			{
				if ((toHit == Both || c->getType() == toHit) && (!ignore || ignore != c) && c->GetGameObject()->GetTag() != ignoreTag)
				{
					RaycastLineCheck(hits, begin, end, { c->box.x, c->box.y }, { c->box.x + c->box.w, c->box.y }, c);
					RaycastLineCheck(hits, begin, end, { c->box.x + c->box.w, c->box.y }, { c->box.x + c->box.w, c->box.y + c->box.h }, c);
					RaycastLineCheck(hits, begin, end, { c->box.x + c->box.w, c->box.y + c->box.h }, { c->box.x, c->box.y + c->box.h }, c);
					RaycastLineCheck(hits, begin, end, { c->box.x, c->box.y + c->box.h }, { c->box.x, c->box.y }, c);
				}

			}
		}
	}
	for (CollisionComponent* c : *m_pDynamicColliders)
	{
		if ((toHit == Both || c->getType() == toHit) && (!ignore || ignore != c) && c->GetGameObject()->GetTag() != ignoreTag)
		{
			RaycastLineCheck(hits, begin, end, { c->box.x, c->box.y }, { c->box.x + c->box.w, c->box.y }, c);
			RaycastLineCheck(hits, begin, end, { c->box.x + c->box.w, c->box.y }, { c->box.x + c->box.w, c->box.y + c->box.h }, c);
			RaycastLineCheck(hits, begin, end, { c->box.x + c->box.w, c->box.y + c->box.h }, { c->box.x, c->box.y + c->box.h }, c);
			RaycastLineCheck(hits, begin, end, { c->box.x, c->box.y + c->box.h }, { c->box.x, c->box.y }, c);
		}
	}
	if (hits.size() == 0)
	{
		return false;
	}

	// Get closest intersection point and copy it into the hitInfo parameter
	hit = *std::min_element(hits.begin(), hits.end(),
		[](const HitInfo& first, const HitInfo& last) {
		return first.lambda < last.lambda; });
	return true;
}

void dae::CollisionManager::RaycastLineCheck(std::vector<HitInfo>& hits, float2 rayBegin, float2 rayEnd, float2 lineBegin, float2 lineEnd, CollisionComponent* obj)
{
	SDL_Rect A;
	A.x = int(rayBegin.x < rayEnd.x ? rayBegin.x : rayEnd.x);
	A.y = int(rayBegin.y < rayEnd.y ? rayBegin.y : rayEnd.y);
	A.w = int(rayBegin.x < rayEnd.x ? rayEnd.x - rayBegin.x : rayBegin.x - rayEnd.x);
	A.h = int(rayBegin.y < rayEnd.y ? rayEnd.y - rayBegin.y : rayBegin.y - rayEnd.y);
	SDL_Rect B;
	B.x = int(lineBegin.x < lineEnd.x ? lineBegin.x : lineEnd.x);
	B.y = int(lineBegin.y < lineEnd.y ? lineBegin.y : lineEnd.y);
	B.w = int(lineBegin.x < lineEnd.x ? lineEnd.x - lineBegin.x : lineBegin.x - lineEnd.x);
	B.h = int(lineBegin.y < lineEnd.y ? lineEnd.y - lineBegin.y : lineBegin.y - lineEnd.y);

	if (IsOverlapping(A, B))
	{
		float lambda1;
		float lambda2;
		if (IntersectLineSegment(rayBegin, rayEnd, lineBegin, lineEnd, lambda1, lambda2))
		{
			if (lambda1 > 0 && lambda1 <= 1 && lambda2 > 0 && lambda2 <= 1)
			{
				HitInfo hitinfo;
				hitinfo.lambda = lambda1;
				hitinfo.point = { rayBegin.x + (rayEnd.x - rayBegin.x) * lambda1, rayBegin.y + (rayEnd.y - rayBegin.y) * lambda1 };
				hitinfo.hit = obj;
				hits.push_back(hitinfo);
			}
		}
	}
}

bool dae::CollisionManager::IntersectLineSegment(float2 p1, float2 p2, float2 q1, float2 q2, float& outLambda1, float& outLambda2, float epsilon)
{
	bool intersecting = false;
	float2 v1{ p2.x - p1.x, p2.y - p1.y };
	float2 v2{ q2.x - q1.x, q2.y - q1.y };

	float denom = v1.CrossProduct(v2);
	if (std::abs(denom) > epsilon)
	{
		intersecting = true;
		float2 begins{ q1.x - p1.x, q1.y - p1.y };

		float num1 = begins.CrossProduct(v2);
		float num2 = begins.CrossProduct(v1);
		outLambda1 = num1 / denom;
		outLambda2 = num2 / denom;
	}
	else
	{
		float2 begins{ q1.x - p1.x, q1.y - p1.y };
		denom = begins.CrossProduct(v2);
		if (std::abs(denom) > epsilon) return false;

		outLambda1 = 0;
		outLambda2 = 0;
		if (IsPointOnLineSegment(p1, q1, q2))intersecting = true;
		if (IsPointOnLineSegment(p2, q1, q2))intersecting = true;
		if (IsPointOnLineSegment(q1, p1, p2))intersecting = true;
		if (IsPointOnLineSegment(q2, p1, p2))intersecting = true;
	}
	return intersecting;

}

bool dae::CollisionManager::IsPointOnLineSegment(float2& p, float2& a, float2& b)
{
	float2 ap{ p.x - a.x, p.y - a.y };
	float2 bp{ p.x - b.x, p.y - b.y };
	if (std::abs(ap.CrossProduct(bp)) > 0.001f )
	{
		return false;
	}
	if (ap.DotProduct(bp) > 0)
	{
		return false;
	}
	return true;
}

bool dae::CollisionManager::IsOverlapping(CollisionComponent* c1, CollisionComponent* c2)
{
	return IsOverlapping(c1->box, c2->box);
 	
}

bool dae::CollisionManager::IsOverlapping(SDL_Rect A, SDL_Rect B)
{
	if ((A.x + A.w) < B.x || (B.x + B.w) < A.x)
	{
		return false;
	}

	if ((A.y + A.h) < B.y || (B.y + B.h) < A.y)
	{
		return false;
	}

	return true;
}
