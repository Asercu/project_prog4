#include "MiniginPCH.h"
#include "PauseScene.h"
#include "TextureComponent.h"
#include "Transform.h"
#include "TextComponent.h"
#include "DataVault.h"

dae::PauseScene::PauseScene(const std::string& SceneName) : Scene(SceneName)
{
	Scene::Init();
	Init();
}

void dae::PauseScene::Init()
{
	auto BG = new GameObject(m_pManagers);
	BG->AddComponent(new TextureComponent(m_pManagers->pComp));
	TextureComponent* texComp = BG->GetComponent<TextureComponent>();
	texComp->SetTexture("background.jpg");
	BG->GetTransform()->SetPosition(0.f, 0.f, -10.f);
	Add(BG);

	auto title = new GameObject(m_pManagers);
	TextComponent* pTextComp = static_cast<TextComponent*>(title->AddComponent(new TextComponent(m_pManagers->pComp)));
	pTextComp->SetText("Pause");
	pTextComp->SetFont("Lingua.otf", 100);
	title->GetTransform()->SetPosition(170.f, 20.f, 0.f);
	Add(title);

	auto resume = new GameObject(m_pManagers);
	m_pResume = static_cast<TextComponent*>(resume->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pResume->SetText("Resume");
	m_pResume->SetFont("Lingua.otf", 50);
	m_pResume->SetColor({ 255, 0, 0, 255 });
	resume->GetTransform()->SetPosition(200.f, 180.f, 0.f);
	Add(resume);

	auto mainmenu = new GameObject(m_pManagers);
	m_pMainMenu = static_cast<TextComponent*>(mainmenu->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pMainMenu->SetText("Main Menu");
	m_pMainMenu->SetFont("Lingua.otf", 50);
	m_pMainMenu->SetColor({ 255, 255, 255, 255 });
	mainmenu->GetTransform()->SetPosition(200.f, 240.f, 0.f);
	Add(mainmenu);

	auto exit = new GameObject(m_pManagers);
	m_pExit = static_cast<TextComponent*>(exit->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pExit->SetText("Quit Game");
	m_pExit->SetFont("Lingua.otf", 50);
	m_pExit->SetColor({ 255, 255, 255, 255 });
	exit->GetTransform()->SetPosition(200, 300.f, 0.f);
	Add(exit);

	std::function<void(const PauseScene&)> prev = std::bind(&PauseScene::Previous, this);
	Command<PauseScene>* cmdPrev = new Command<PauseScene>(this);
	cmdPrev->device = InputDevice::Controller;
	cmdPrev->controllerID = 0;
	cmdPrev->action = InputAction::Pressed;
	cmdPrev->btn = ControllerButton::DPadUP;
	cmdPrev->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrev);

	std::function<void(const PauseScene&)> next = std::bind(&PauseScene::Next, this);
	Command<PauseScene>* cmdNext = new Command<PauseScene>(this);
	cmdNext->device = InputDevice::Controller;
	cmdNext->controllerID = 0;
	cmdNext->action = InputAction::Pressed;
	cmdNext->btn = ControllerButton::DPadDown;
	cmdNext->fn = next;
	m_pManagers->pInput->AddAction(cmdNext);

	std::function<void(const PauseScene&)> select = std::bind(&PauseScene::Select, this);
	Command<PauseScene>* cmdSelect = new Command<PauseScene>(this);
	cmdSelect->device = InputDevice::Controller;
	cmdSelect->controllerID = 0;
	cmdSelect->action = InputAction::Pressed;
	cmdSelect->btn = ControllerButton::ButtonA;
	cmdSelect->fn = select;
	m_pManagers->pInput->AddAction(cmdSelect);

	//std::function<void(const MainmenuScene&)> prev = std::bind(&MainmenuScene::Previous, this);
	Command<PauseScene>* cmdPrevKB = new Command<PauseScene>(this);
	cmdPrevKB->device = InputDevice::KeyBoard;
	cmdPrevKB->action = InputAction::Pressed;
	cmdPrevKB->key = SDL_SCANCODE_UP;
	cmdPrevKB->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrevKB);

	//std::function<void(const MainmenuScene&)> next = std::bind(&MainmenuScene::Next, this);
	Command<PauseScene>* cmdNextKB = new Command<PauseScene>(this);
	cmdNextKB->device = InputDevice::KeyBoard;
	cmdNextKB->action = InputAction::Pressed;
	cmdNextKB->key = SDL_SCANCODE_DOWN;
	cmdNextKB->fn = next;
	m_pManagers->pInput->AddAction(cmdNextKB);

	//std::function<void(const MainmenuScene&)> select = std::bind(&MainmenuScene::Select, this);
	Command<PauseScene>* cmdSelectKB = new Command<PauseScene>(this);
	cmdSelectKB->device = InputDevice::KeyBoard;
	cmdSelectKB->action = InputAction::Pressed;
	cmdSelectKB->key = SDL_SCANCODE_RETURN;
	cmdSelectKB->fn = select;
	m_pManagers->pInput->AddAction(cmdSelectKB);
}

void dae::PauseScene::Update()
{
	Scene::Update();
}

void dae::PauseScene::Previous()
{
	if (--selectedIndex < 0) selectedIndex = 0;
	SetColors();
}

void dae::PauseScene::Next()
{
	if (++selectedIndex > 2) selectedIndex = 2;
	SetColors();
}

void dae::PauseScene::Select()
{
	switch (selectedIndex)
	{
	case 0:
		SceneManager::GetInstance().SetActiveScene(DataVault::GetInstance().pausedScene);
		break;
	case 1:
		SceneManager::GetInstance().DeleteScene(DataVault::GetInstance().pausedScene);
		SceneManager::GetInstance().SetActiveScene("main");
		break;
	case 2:
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
		break;
	}
}

void dae::PauseScene::SetColors()
{
	SDL_Color red = { 255,0,0,255 };
	SDL_Color white = { 255,255,255,255 };

	m_pResume->SetColor(selectedIndex == 0 ? red : white);
	m_pMainMenu->SetColor(selectedIndex == 1 ? red : white);
	m_pExit->SetColor(selectedIndex == 2 ? red : white);
}

