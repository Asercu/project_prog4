#include "MiniginPCH.h"
#include "Block.h"
#include "CollisionComponent.h"
#include "TextureComponent.h"

dae::Block::Block(SceneManagers* pManagers) : GameObject(pManagers)
{
	CollisionComponent* pCollider = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape CS;
	SDL_Rect box;
	box.x = 0;
	box.y = 0;
	box.w = 32;
	box.h = 32;
	CS.box = box;
	CS.dynamic = false;
	CS.type = Collider;
	pCollider->SetShape(CS);
	TextureComponent* ptexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	ptexComp->SetTexture("block.png");
}
