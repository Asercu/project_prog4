#include "MiniginPCH.h"
#include "Transform.h"
#include "ComponentManager.h"
#include "CollisionComponent.h"
#include "Renderer.h"

void dae::Transform::Update(Transform* parentTransform)
{
	if (parentTransform == nullptr)
	{
		GameObject* pParent = GetGameObject()->GetParentGameObject();
		if (pParent) parentTransform = pParent->GetTransform();
	}
	if (parentTransform == nullptr)
	{
		m_WorldPosition = m_Position;
		m_WorldAngle = m_Angle;
		m_WorldScale = m_Scale;
	}
	else
	{
		m_WorldPosition = parentTransform->GetWorldPosition() + m_Position;
		m_WorldAngle = parentTransform->GetWorldAngle() + m_Angle;
		m_WorldScale = parentTransform->GetWorldScale() + m_Scale;
	}
	auto collider = GetGameObject()->GetComponent<CollisionComponent>();
	if (!collider)
	{
		UpdateChildren();
	}
	else if (collider->GetDynamic())
	{
		collider->UpdateBox();
		UpdateChildren();
	}
	else
	{
		throw std::exception("ERROR: TransForm::Update() > moving object with static collider");
	}
}

void dae::Transform::UpdateChildren()
{
	for (GameObject* pChild : GetGameObject()->GetChildren())
	{
		pChild->GetTransform()->Update(this);
	}
}

void dae::Transform::SetPosition(const float x, const float y, const float z)
{
	auto collider = GetGameObject()->GetComponent<CollisionComponent>();
	if (collider && !collider->GetDynamic())
	{
		std::cout << "ERROR: trying to move object with static collider" << std::endl;
		return;
	}
	bool mustSort = false;
	if (z != m_Position.z) mustSort = true;
	m_Position = glm::vec3(x, y, z);
	Update();
	Scene* pS = GetGameObject()->GetParentScene();
	if (pS && mustSort) pS->GetComponentManager()->SortRenderComponents();
}

void dae::Transform::SetPosition(glm::vec3 pos)
{
	SetPosition(pos.x, pos.y, pos.z);
}

void dae::Transform::SetAngle(float angle)
{
	m_Angle = angle;
	Update();
}

void dae::Transform::SetScale(float x, float y, float z)
{
	m_Scale = glm::vec3(x, y, z);
	Update();
}

void dae::Transform::SetScale(glm::vec3 scale)
{
	m_Scale = scale;
	Update();
}

glm::vec3 dae::Transform::GetPosition() const
{
	return m_Position;
}

float dae::Transform::GetAngle() const
{
	return m_Angle;
}

glm::vec3 dae::Transform::GetScale() const
{
	return m_Scale;
}

glm::vec3 dae::Transform::GetWorldPosition() const
{
	return m_WorldPosition;
}

float dae::Transform::GetWorldAngle() const
{
	return m_WorldAngle;
}

glm::vec3 dae::Transform::GetWorldScale() const
{
	return m_WorldScale;
}

void dae::Transform::Move(float x, float y, float z, bool collision, std::string ignoreTag)
{
	/*
	if (!collision)
	{
		SetPosition(m_Position.x + x, m_Position.y + y, m_Position.z + z);
		return;
	}
	CollisionComponent* collider = GetGameObject()->GetComponent<CollisionComponent>();
	CollisionManager* pColMan = collider->GetCollisionManager();
	HitInfo hit;
	if (pColMan->RayCast(collider->getColliderCenter(), { collider->getColliderCenter().x + x, collider->getColliderCenter().y + y }, hit, Collider, nullptr, ignoreTag))
	{
		if (hit.hit == collider)
		{
			HitInfo hit2;
			if (pColMan->RayCast(collider->getColliderCenter(), { hit.point.x + x, hit.point.y + y }, hit2, Collider, collider))
			{
				float distance = collider->getColliderCenter().Distance(hit2.point) - collider->getColliderCenter().Distance(hit.point);
				float2 movement{ x, y };
				movement = movement.Normalized();
				SetPosition(m_Position.x + distance * movement.x, m_Position.y + distance * movement.y, m_Position.z + z);
				return;
			}
			else
			{
				SetPosition(m_Position.x + x, m_Position.y + y, m_Position.z + z);
				return;
			}
		}
		return;
	}
	SetPosition(m_Position.x + x, m_Position.y + y, m_Position.z + z);
	*/
	if (!collision)
	{
		SetPosition(m_Position.x + x, m_Position.y + y, m_Position.z + z);
		return;
	}
	CollisionComponent* collider = GetGameObject()->GetComponent<CollisionComponent>();
	CollisionManager* pColMan = collider->GetCollisionManager();
	HitInfo hit;
	float2 direction = { x, y };
	//float displacement = direction.getLength();
	direction = direction.Normalized();
	float2 center = collider->getColliderCenter();
	if (pColMan->RayCast(center, { center.x + direction.x * 1000, center.y + direction.y * 1000 }, hit, collider))
	{
		HitInfo hitAll;
		pColMan->RayCast(center, { center.x + direction.x * 1000, center.y + direction.y * 1000 }, hitAll, Collider, collider);
		if (hitAll.lambda < hit.lambda) return;

		HitInfo hit2;
		//if (pColMan->RayCast(hit.point, { hit.point.x + direction.x * (displacement - 1000 * hit.lambda), hit.point.y + direction.y * (displacement - 1000 * hit.lambda) }, hit2, Collider, collider, ignoreTag))
		if (pColMan->RayCast({ hit.point.x - direction.x, hit.point.y - direction.y }, { hit.point.x + x, hit.point.y + y }, hit2, Collider, collider, ignoreTag))
		{
			//SetPosition(m_Position.x - direction.x + x * hit2.lambda, m_Position.y - direction.y + y * hit2.lambda, m_Position.z + z);
			float displacementX = x * hit2.lambda;
			float displacementY = y * hit2.lambda;
			SetPosition((abs(displacementX) <= 2.f ? m_Position.x : m_Position.x + displacementX), (abs(displacementY) <= 2.f ? m_Position.y : m_Position.y + displacementY) , m_Position.z + z);
			return;
		}
		else
		{
			SetPosition(m_Position.x + x, m_Position.y + y, m_Position.z + z);
		}
	}
}

dae::Transform::Transform(ComponentManager* pCompManager)
{
	m_Position = glm::vec3(0.f, 0.f, 0.f);
	m_Angle = 0.f;
	m_Scale = glm::vec3(1.f, 1.f, 1.f);

	pCompManager->AddComponent(this);
}
