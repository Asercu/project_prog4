#pragma once
#include "Singleton.h"
#include <chrono>

namespace dae
{
	class Time final : public Singleton<Time>
	{
	private:
		std::chrono::high_resolution_clock::time_point m_CurrentTime;
		std::chrono::high_resolution_clock::time_point m_PreviousTime;
		bool m_UseFixedTime = false;
		float m_FixedInterval = 16.f;
		float m_DeltaT;

	public:
		float GetDeltaT() const;
		void Initialize();
		void Update();
		void SetEnableFixedTime(bool enable);
		void SetFixedInterval(float ms);
		unsigned int GetFPS() const;
	};
}
