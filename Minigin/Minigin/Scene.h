#pragma once
#include "SceneManager.h"
#include "ComponentManager.h"
#include "InputManager.h"
#include "CollisionManager.h"

namespace dae
{	
	class InputManager;
	class GameObject;
	class CollisionManager;

	struct SceneManagers
	{
		ComponentManager* pComp;
		InputManager* pInput;
		CollisionManager* pCollision;
	};

	class Scene
	{
		friend Scene& SceneManager::CreateScene(const std::string& name);
	public:
		void Add(GameObject* object);

		bool HandleInput();
		virtual void Update();
		void Render();
		virtual void Init();
		std::string GetName() const { return m_Name; };
		ComponentManager* GetComponentManager() const { return m_pManagers->pComp; };
		InputManager* GetInputmanager() const { return m_pManagers->pInput; };
		CollisionManager* GetCollisionManager() const { return m_pManagers->pCollision; };

		~Scene();
		Scene(const Scene& other) = delete;
		Scene(Scene&& other) = delete;
		Scene& operator=(const Scene& other) = delete;
		Scene& operator=(Scene&& other) = delete;

	protected: 
		explicit Scene(const std::string& name);

		std::string m_Name{};
		std::vector <GameObject*> m_Objects{};
		std::vector <GameObject*> m_ThreadedObjects{};

		SceneManagers* m_pManagers;

		static unsigned int idCounter; 
	};

}
