#include "MiniginPCH.h"
#include "DemoScene.h"
#include "ResourceManager.h"
#include "GameObject.h"
#include "TextureComponent.h"
#include "TextComponent.h"
#include "Time.h"
#include "Transform.h"
#include "Pacman.h"
#include "CollisionComponent.h"
#include "Ghost.h"
#include "DataVault.h"
#include "Block.h"

dae::DemoScene::DemoScene(const std::string& name) : Scene(name)
{
	Init();
}

void dae::DemoScene::Init()
{
	Scene::Init();

	auto go1 = new GameObject(m_pManagers);
	go1->AddComponent(new TextureComponent(m_pManagers->pComp));
	TextureComponent* texComp = go1->GetComponent<TextureComponent>();
	texComp->SetTexture("background.jpg");
	go1->GetTransform()->SetPosition(0.f, 0.f, -10.f);
	Add(go1);

	Logo = new GameObject(m_pManagers);
	TextureComponent* texComp2 = static_cast<TextureComponent*>(Logo->AddComponent(new TextureComponent(m_pManagers->pComp)));
	//TextureComponent* texComp2 = Logo->GetComponent<TextureComponent>();
	texComp2->SetTexture("logo.png");
	Logo->GetTransform()->SetPosition(256.f, 256.f, 5.f);
	//Logo->AddComponent(new InputComponent(m_pCompManager));
	//InputComponent* InputComp1 = Logo->GetComponent<InputComponent>();
	//InputComp1->AddCommand();
	CollisionComponent* pcollider = static_cast<CollisionComponent*>(Logo->AddComponent(new CollisionComponent(m_pManagers)));
	ColliderShape cs;
	cs.box.x = 50;
	cs.box.y = 0;
	cs.box.w = 100;
	cs.box.h = 128;
	cs.dynamic = false;
	pcollider->SetShape(cs);
	Add(Logo);
	//go1->AddChild(Logo);
	
	auto go = new GameObject(m_pManagers);
	go->AddComponent(new TextComponent(m_pManagers->pComp));
	m_pTextComp = go->GetComponent<TextComponent>();
	m_pTextComp->SetFont("Lingua.otf", 36);
	m_pTextComp->SetText(m_Name);
	m_pTextComp->SetColor({ 255, 0, 0 });
	go->GetTransform()->SetPosition(20.f, 20.f, 0);
	Add(go);

	auto pacman = new Pacman(m_pManagers, 1);
	pacman->GetTransform()->SetPosition(50.f, 50.f, 5.f);
	Add(pacman);

	auto red = new Ghost(m_pManagers, Ghost::Red);
	red->GetTransform()->SetPosition(50.f, 0.f, 0.f);
	Add(red);
	auto orange = new Ghost(m_pManagers, Ghost::Orange);
	orange->GetTransform()->SetPosition(50.f, 50.f, 0.f);
	Add(orange);
	auto pink = new Ghost(m_pManagers, Ghost::Orange);
	pink->GetTransform()->SetPosition(100.f, 0.f, 0.f);
	Add(pink);
	auto blue = new Ghost(m_pManagers, Ghost::Blue);
	blue->GetTransform()->SetPosition(100.f, 50.f, 0.f);
	Add(blue);
	for (int i = 0; i < DataVault::GetInstance().windowWidth; i += 32)
	{
		auto block = new Block(m_pManagers);
		block->GetTransform()->SetPosition(float(i), 0.f, 0.f);
		Add(block);
		auto block2 = new Block(m_pManagers);
		block2->GetTransform()->SetPosition(float(i), DataVault::GetInstance().windowHeight - 32.0f, 0.f);
		Add(block2);
	}
	for (int i = 32; i < DataVault::GetInstance().windowHeight - 32; i += 32)
	{
		auto block = new Block(m_pManagers);
		block->GetTransform()->SetPosition(0.f, float(i), 0.f);
		Add(block);
		auto block2 = new Block(m_pManagers);
		block2->GetTransform()->SetPosition(DataVault::GetInstance().windowWidth - 32.0f,  float(i), 0.f);
		Add(block2);
	}

}

void dae::DemoScene::Update()
{
	m_pTextComp->SetText("FPS:" + std::to_string(Time::GetInstance().GetFPS()));
	//Logo->GetTransform()->SetAngle(Logo->GetTransform()->GetAngle() + 50.f * Time::GetInstance().GetDeltaT());
	m_pTextComp->GetGameObject()->GetTransform()->SetAngle(m_pTextComp->GetGameObject()->GetTransform()->GetAngle() + 50.f * Time::GetInstance().GetDeltaT());


	Scene::Update();
}
