#pragma once
#include <string>
#include <fstream>
#include <vector>

class BinaryReader
{
public:
	BinaryReader(std::string filename)
	{
		m_File = filename;
	};

	template <typename T>
	T Read()
	{
		if (!in.is_open()) throw std::exception("error in BinaryReader::Read: filestream not open");
		T value;
		in.read((char*)&value, sizeof(T));
		return value;
	};

	template <>
	std::string Read<std::string>()
	{
		if (!in.is_open()) throw std::exception("error in BinaryReader::Read: filestream not open");
		short size;
		in.read((char *)&size, sizeof(short));
		std::string value;
		value.resize(size);
		for (int i = 0; i < size; ++i)
		{
			in.read((char *)&value[i], sizeof(value[i]));
		}
		return value;
	}

	template <typename T>
	std::vector<T> ReadVector()
	{
		if (!in.is_open()) throw std::exception("error in BinaryReader::ReadVector: filestream not open");
		short size;
		in.read((char *)&size, sizeof(short));
		std::vector<T> value;
		value.resize(size);
		for (int i = 0; i < size; ++i)
		{
			value[i] = Read<T>();
		}
		return value;
	}

	void open()
	{
		if (!in.is_open())	in.open(m_File, std::ios::out | std::ios::binary);
	}

	void close()
	{
		if (in.is_open()) in.close();
	}


private:
	std::string m_File;
	std::ifstream in;
};