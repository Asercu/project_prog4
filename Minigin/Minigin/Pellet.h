#pragma once
#include "GameObject.h"

namespace dae
{
	class Pellet : public GameObject
	{
	public:
		Pellet(SceneManagers* pManagers);
		void Update() override;
	private:
		CollisionComponent * m_pCollider;
	};
}