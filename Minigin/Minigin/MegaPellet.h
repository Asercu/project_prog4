#pragma once
#include "GameObject.h"

namespace dae
{
	class TextureComponent;
	class MegaPellet : public GameObject
	{
	public:
		MegaPellet(SceneManagers* pManagers);
		void Update() override;

	private:
		float m_Timer = 0.f;
		bool m_IsYellow = true;
		float m_ColorTime = .6f;
		TextureComponent* m_pTexComp;
		CollisionComponent* m_pCollider;
	};
}