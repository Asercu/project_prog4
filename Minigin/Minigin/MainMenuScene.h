#pragma once
#include "Scene.h"

namespace dae
{
	class TextComponent;

	class MainmenuScene : public Scene
	{
	public:
		MainmenuScene(const std::string& SceneName);
		void Init() override;
		void Update() override;

		void Previous();
		void Next();
		void Select();

	private:
		int selectedIndex = 0;
		TextComponent* m_pSingle;
		TextComponent* m_pDouble;
		TextComponent* m_pGhost;
		TextComponent* m_pExit;

		void SetColors();
	};
}