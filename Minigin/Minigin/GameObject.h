#pragma once
#include <memory>
#include "Texture2D.h"
#include "Scene.h"

namespace dae
{
	class Transform;
	class Component;
	struct SceneManagers;
	class CommandInterface;

	class GameObject
	{
	public:
		virtual void Update();
		virtual void Render() const;
		Component* AddComponent(Component* pComponent);
		Transform* GetTransform() { return m_Transform; };
		std::vector<GameObject*>& GetChildren() { return m_pChildren; };
		GameObject* GetParentGameObject() const { return static_cast<GameObject*>(m_pParent); };
		Scene* GetParentScene() const;
		void SetParent(GameObject* parent);
		void SetParent(Scene* scene);
		void AddChild(GameObject* child);
		std::string GetTag() { return m_Tag; };
		void SetActive(bool enable);
		bool IsActive() { return m_IsActive; };
		bool IsThreaded() { return m_Threaded; };

		template<typename T>
		T* GetComponent();

		//GameObject();
		GameObject(Transform* transform);
		GameObject(SceneManagers* managers, std::string tag = "", bool threaded = false);
		virtual ~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	protected:
		Transform * m_Transform = nullptr;
		std::string m_Tag = "";
		std::vector<CommandInterface*> m_Commands{};

	private:
		std::vector<Component*> m_pComponents;
		std::vector<GameObject*> m_pChildren;
		GameObject* m_pParent;
		Scene* m_pParentScene;
		bool m_IsActive = true;
		bool m_Threaded = false;
	};

	template <typename T>
	T* GameObject::GetComponent()
	{
		for (auto* c : m_pComponents)
		{
			T* returnvalue = dynamic_cast<T*>(c);
			if (returnvalue != nullptr) return returnvalue;
		}
		return nullptr;
	}
}
