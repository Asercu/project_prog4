#include "MiniginPCH.h"
#include "Pellet.h"
#include "CollisionComponent.h"
#include "TextureComponent.h"
#include "PacmanScene.h"

dae::Pellet::Pellet(SceneManagers* pManagers) : GameObject(pManagers)
{
	m_pCollider = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape CS;
	SDL_Rect box;
	box.x = 13;
	box.y = 13;
	box.h = 6;
	box.w = 6;
	CS.box = box;
	CS.dynamic = false;
	CS.type = Trigger;
	m_pCollider->SetShape(CS);
	TextureComponent* pTexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	pTexComp->SetTexture("Collectible.png");
}

void dae::Pellet::Update()
{
	GameObject::Update();
	auto collisions = m_pCollider->GetOverLappingObjects();
	for (GameObject* pG : *collisions)
	{
		if (pG->GetTag() == "Pacman")
		{
			SetActive(false);
			
			Scene* sc = GetParentScene();
			PacmanScene* s = static_cast<PacmanScene*>(sc);
			s->PelletCollected(pG);
		}
	}
	delete collisions;
}
