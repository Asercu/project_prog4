#include "MiniginPCH.h"
#include "Time.h"
#include <chrono>

void dae::Time::Initialize()
{
	m_PreviousTime = std::chrono::high_resolution_clock::now();
}

void dae::Time::Update()
{
	m_PreviousTime = m_CurrentTime;
	m_CurrentTime = std::chrono::high_resolution_clock::now();
	if (!m_UseFixedTime)
	{
		m_DeltaT = std::chrono::duration_cast<std::chrono::milliseconds>(m_CurrentTime - m_PreviousTime).count() / 1000.f;
		//m_DeltaT = std::chrono::duration<float>(m_CurrentTime - m_PreviousTime).count();
	}
	else
	{
		m_DeltaT = m_FixedInterval;
	}

}

void dae::Time::SetEnableFixedTime(bool enable)
{
	m_UseFixedTime = enable;
}

float dae::Time::GetDeltaT() const
{
	return m_DeltaT;
}

void dae::Time::SetFixedInterval(float ms)
{
	m_FixedInterval = ms;
}

unsigned int dae::Time::GetFPS() const
{
	return static_cast<unsigned int>(1 / m_DeltaT);
}

