#include "MiniginPCH.h"
#include "Ghost.h"
#include "Transform.h"
#include "CollisionComponent.h"
#include "Time.h"
#include "PacmanScene.h"
#include "Pacman.h"
#include <algorithm>

dae::Ghost::Ghost(SceneManagers* pManagers, Colors c) : GameObject(pManagers)
{
	m_pTexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	m_Color = c;
	SetTexture();
	GetTransform()->SetScale(.25f, .25f, .25f);
	m_pCollider = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape CS;
	SDL_Rect box;
	box.x = 1;
	box.h = 30;
	box.w = 30;
	box.y = 1;
	CS.box = box;
	CS.dynamic = true;
	CS.type = Trigger;
	m_pCollider->SetShape(CS);

	std::function<void(const Ghost&)> GhostMove = std::bind(&Ghost::Move, this);
	Command<Ghost>* cmdMove = new Command<Ghost>(this);
	m_Commands.push_back(cmdMove);
	cmdMove->device = InputDevice::AutoExecute;
	cmdMove->fn = GhostMove;
	cmdMove->Threaded = true;
	pManagers->pInput->AddAction(cmdMove);
}

void dae::Ghost::MegaPelletCollected()
{
	m_State = Killable;
	m_pTexComp->SetTexture("killable.gif");
	m_Timer = 0.f;
}

void dae::Ghost::Update()
{
	auto collisions = m_pCollider->GetOverLappingObjects();
	for (GameObject* pObj : *collisions)
	{
		if (pObj->GetTag() == "Pacman")
		{
			if (m_State == Killable)
			{
				static_cast<PacmanScene*>(GetParentScene())->AddScore(200, pObj);
				m_State = Dead;
				m_pTexComp->SetTexture("dead.png");
				m_Timer = 0.f;
			}
			else if (m_State == Dead)
			{
				
			}
			else
			{
				static_cast<PacmanScene*>(GetParentScene())->KillPacman(pObj);
			}
		}
	}
	delete collisions;

	m_Timer += Time::GetInstance().GetDeltaT();

	switch (m_State)
	{
	case Killable:
		if (m_Timer >= 3.f)
		{
			m_Timer = 0.f;
			m_State = none;
			SetTexture();
		}
		break;
	case Dead:
		if (m_Timer >= 3.f)
		{
			m_Timer = 0.f;
			m_State = none;
			SetTexture();
		}
		break;
	}
}

void dae::Ghost::Move()
{
	float2 center = m_pCollider->getColliderCenter();
	HitInfo up, down, left, right;
	m_pCollider->GetCollisionManager()->RayCast(center, { center.x + 500.f, center.y }, right, Collider, m_pCollider);
	m_pCollider->GetCollisionManager()->RayCast(center, { center.x - 500.f, center.y }, left, Collider, m_pCollider);
	m_pCollider->GetCollisionManager()->RayCast(center, { center.x, center.y + 500.f }, down, Collider, m_pCollider);
	m_pCollider->GetCollisionManager()->RayCast(center, { center.x, center.y - 500.f }, up, Collider, m_pCollider);
	
	if (right.hit != nullptr && dynamic_cast<Pacman*>(right.hit->GetGameObject()) != nullptr)
	{
		if (m_State == none)
		{
			MoveRight();
			m_CurrentDir = Right;
			return;
		}
		else
		{
			MoveLeft();
			m_CurrentDir = Left;
			return;
		}

	}
	if (left.hit != nullptr && dynamic_cast<Pacman*>(left.hit->GetGameObject()) != nullptr)
	{
		if (m_State == none)
		{
			MoveLeft();
			m_CurrentDir = Left;
			return;
		}
		else
		{
			MoveRight();
			m_CurrentDir = Right;
			return;
		}
		
	}
	if (up.hit != nullptr && dynamic_cast<Pacman*>(up.hit->GetGameObject()) != nullptr)
	{
		if (m_State == none)
		{
			MoveUp();
			m_CurrentDir = Up;
			return;
		}
		else
		{
			MoveDown();
			m_CurrentDir = Down;
			return;
		}

	}
	if (down.hit != nullptr && dynamic_cast<Pacman*>(down.hit->GetGameObject()) != nullptr)
	{
		if (m_State == none)
		{
			MoveDown();
			m_CurrentDir = Down;
			return;
		}
		else
		{
			MoveUp();
			m_CurrentDir = Up;
		}
		
	}

	//continue moving in current direction
	switch (m_CurrentDir)
	{
	case NONE:
	{
		std::vector<HitInfo> hits{ up, down, left, right };
		HitInfo furthest = *std::max_element(hits.begin(), hits.end(),
		[](const HitInfo& first, const HitInfo& last) {
		return first.lambda < last.lambda; });
		float2 dir = { furthest.point.x - center.x, furthest.point.y - center.y };
		float2 pos = { GetTransform()->GetWorldPosition().x, GetTransform()->GetWorldPosition().y };
		m_Transform->Move(dir.Normalized().x * speed * Time::GetInstance().GetDeltaT(), dir.Normalized().y * speed * Time::GetInstance().GetDeltaT(), 3.f, true);
		if (GetTransform()->GetWorldPosition().x > pos.x) m_CurrentDir = Right;
		if (GetTransform()->GetWorldPosition().x < pos.x) m_CurrentDir = Left;
		if (GetTransform()->GetWorldPosition().y > pos.y) m_CurrentDir = Down;
		if (GetTransform()->GetWorldPosition().y < pos.y) m_CurrentDir = Up;		
	}	
	break;
	case Down:
	{
		float2 pos = { GetTransform()->GetWorldPosition().x, GetTransform()->GetWorldPosition().y };
		MoveDown();
		if (GetTransform()->GetWorldPosition().x == pos.x && GetTransform()->GetWorldPosition().y == pos.y)
		m_CurrentDir = NONE;
	}		
	break;
	case Up:
	{
		float2 pos = { GetTransform()->GetWorldPosition().x, GetTransform()->GetWorldPosition().y };
		MoveUp();
		if (GetTransform()->GetWorldPosition().x == pos.x && GetTransform()->GetWorldPosition().y == pos.y)
		m_CurrentDir = NONE;
	}
	break;
	case Left:
	{
		float2 pos = { GetTransform()->GetWorldPosition().x, GetTransform()->GetWorldPosition().y };
		MoveLeft();
		if (GetTransform()->GetWorldPosition().x == pos.x && GetTransform()->GetWorldPosition().y == pos.y)
			m_CurrentDir = NONE;
	}
	break;
	case Right:
	{
		float2 pos = { GetTransform()->GetWorldPosition().x, GetTransform()->GetWorldPosition().y };
		MoveRight();
		if (GetTransform()->GetWorldPosition().x == pos.x && GetTransform()->GetWorldPosition().y == pos.y)
		m_CurrentDir = NONE;
	}
	break;
	}
}

void dae::Ghost::SetTexture()
{
	switch (m_Color)
	{
	case Red:
		m_pTexComp->SetTexture("red.gif");
		break;
	case Orange:
		m_pTexComp->SetTexture("orange.gif");
		break;
	case Pink:
		m_pTexComp->SetTexture("pink.gif");
		break;
	case Blue:
		m_pTexComp->SetTexture("blue.gif");
		break;
	}
}

void dae::Ghost::MoveRight()
{
	m_Transform->Move(speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true, "Pacman");
}

void dae::Ghost::MoveLeft()
{
	m_Transform->Move(-speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true, "Pacman");
}
void dae::Ghost::MoveUp()
{
	m_Transform->Move(0.f, -speed * Time::GetInstance().GetDeltaT(), 0.f, true, "Pacman");
}
void dae::Ghost::MoveDown()
{
	m_Transform->Move(.0f * Time::GetInstance().GetDeltaT(), speed * Time::GetInstance().GetDeltaT(), 0.f, true, "Pacman");
}
