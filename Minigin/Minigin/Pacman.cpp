#include "MiniginPCH.h"
#include "Pacman.h"
#include "Transform.h"
#include "Time.h"
#include <functional>
#include "InputManager.h"
#include "CollisionComponent.h"

dae::Pacman::Pacman(SceneManagers* pManagers, int controllerID) : GameObject(pManagers, "Pacman")
{
	m_ControllerID = controllerID;
	pTexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	pTexComp->SetTexture(m_ControllerID == 0 ? "pacman.gif" : "mspacman.png");

	CollisionComponent* pColliderComp = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape cs;
	cs.dynamic = true;
	SDL_Rect r;
	r.x = (int)GetTransform()->GetPosition().x;
	r.y = (int)GetTransform()->GetPosition().y;
	r.h = 32;
	r.w = 32;
	cs.box = r;
	cs.type = Collider;
	pColliderComp->SetShape(cs);

	//controller
	std::function<void(const Pacman&)> pacmanMoveUP = std::bind(&Pacman::MoveUp, this);
	Command<Pacman>* cmdUP = new Command<Pacman>(this);
	m_Commands.push_back(cmdUP);
	cmdUP->device = InputDevice::Controller;
	cmdUP->controllerID = controllerID;
	cmdUP->action = InputAction::Pressed;
	cmdUP->btn = ControllerButton::DPadUP;
	cmdUP->fn = pacmanMoveUP;
	pManagers->pInput->AddAction(cmdUP);

	std::function<void(const Pacman&)> pacmanMoveDown = std::bind(&Pacman::MoveDown, this);
	Command<Pacman>* cmdDOWN = new Command<Pacman>(this);
	m_Commands.push_back(cmdDOWN);
	cmdDOWN->device = InputDevice::Controller;
	cmdDOWN->controllerID = controllerID;
	cmdDOWN->action = InputAction::Pressed;
	cmdDOWN->btn = ControllerButton::DPadDown;
	cmdDOWN->fn = pacmanMoveDown;
	pManagers->pInput->AddAction(cmdDOWN);

	std::function<void(const Pacman&)> pacmanMoveRight = std::bind(&Pacman::MoveRight, this);
	Command<Pacman>* cmdRIGHT = new Command<Pacman>(this);
	m_Commands.push_back(cmdRIGHT);
	cmdRIGHT->device = InputDevice::Controller;
	cmdRIGHT->controllerID = controllerID;
	cmdRIGHT->action = InputAction::Pressed;
	cmdRIGHT->btn = ControllerButton::DPadRight;
	cmdRIGHT->fn = pacmanMoveRight;
	pManagers->pInput->AddAction(cmdRIGHT);

	std::function<void(const Pacman&)> pacmanMoveLeft = std::bind(&Pacman::MoveLeft, this);
	Command<Pacman>* cmdLEFT = new Command<Pacman>(this);
	m_Commands.push_back(cmdLEFT);
	cmdLEFT->device = InputDevice::Controller;
	cmdLEFT->controllerID = controllerID;
	cmdLEFT->action = InputAction::Pressed;
	cmdLEFT->btn = ControllerButton::DPadLeft;
	cmdLEFT->fn = pacmanMoveLeft;
	pManagers->pInput->AddAction(cmdLEFT);

	//Keyboard
	//std::function<void(const Pacman&)> pacmanMoveUP = std::bind(&Pacman::MoveUp, this);
	Command<Pacman>* cmdUPKB = new Command<Pacman>(this);
	m_Commands.push_back(cmdUPKB);
	cmdUPKB->device = InputDevice::KeyBoard;
	cmdUPKB->action = InputAction::Pressed;
	cmdUPKB->key = controllerID == 0 ? SDL_SCANCODE_W : SDL_SCANCODE_UP;
	cmdUPKB->fn = pacmanMoveUP;
	pManagers->pInput->AddAction(cmdUPKB);

	//std::function<void(const Pacman&)> pacmanMoveDown = std::bind(&Pacman::MoveDown, this);
	Command<Pacman>* cmdDOWNKB = new Command<Pacman>(this);
	m_Commands.push_back(cmdDOWNKB);
	cmdDOWNKB->device = InputDevice::KeyBoard;
	cmdDOWNKB->action = InputAction::Pressed;
	cmdDOWNKB->key = controllerID == 0 ? SDL_SCANCODE_S : SDL_SCANCODE_DOWN;
	cmdDOWNKB->fn = pacmanMoveDown;
	pManagers->pInput->AddAction(cmdDOWNKB);

	//std::function<void(const Pacman&)> pacmanMoveRight = std::bind(&Pacman::MoveRight, this);
	Command<Pacman>* cmdRIGHTKB = new Command<Pacman>(this);
	m_Commands.push_back(cmdRIGHTKB);
	cmdRIGHTKB->device = InputDevice::KeyBoard;
	cmdRIGHTKB->action = InputAction::Pressed;
	cmdRIGHTKB->key = controllerID == 0 ? SDL_SCANCODE_D : SDL_SCANCODE_RIGHT;
	cmdRIGHTKB->fn = pacmanMoveRight;
	pManagers->pInput->AddAction(cmdRIGHTKB);

	//std::function<void(const Pacman&)> pacmanMoveLeft = std::bind(&Pacman::MoveLeft, this);
	Command<Pacman>* cmdLEFTKB = new Command<Pacman>(this);
	m_Commands.push_back(cmdLEFTKB);
	cmdLEFTKB->device = InputDevice::KeyBoard;
	cmdLEFTKB->action = InputAction::Pressed;
	cmdLEFTKB->key = controllerID == 0? SDL_SCANCODE_A : SDL_SCANCODE_LEFT;
	cmdLEFTKB->fn = pacmanMoveLeft;
	pManagers->pInput->AddAction(cmdLEFTKB);

	std::function<void(const Pacman&)> pacmanMove = std::bind(&Pacman::Move, this);
	Command<Pacman>* cmdMOVE = new Command<Pacman>(this);
	m_Commands.push_back(cmdMOVE);
	cmdMOVE->device = InputDevice::AutoExecute;
	cmdMOVE->fn = pacmanMove;
	pManagers->pInput->AddAction(cmdMOVE);
}

void dae::Pacman::MoveRight()
{
	m_dir = Right;
	//m_Transform->Move(speed, 0.f, 0.f, true);
	//m_Transform->SetAngle(0.f);
}

void dae::Pacman::MoveLeft()
{
	m_dir = Left;
	//m_Transform->Move(-speed, 0.f, 0.f, true);
	//m_Transform->SetAngle(180.f);
}
void dae::Pacman::MoveUp()
{
	m_dir = Up;
	//m_Transform->Move(0.f, -speed, 0.f, true);
	//m_Transform->SetAngle(270.f);
}
void dae::Pacman::MoveDown()
{
	m_dir = Down;
	//m_Transform->Move(.0f, speed, 0.f, true);
	//m_Transform->SetAngle(90.f);
}

void dae::Pacman::Move()
{
	//return;
	
	switch (m_dir)
	{
	case NONE:
		break;
	case Up:
		m_Transform->Move(0.f, -speed * Time::GetInstance().GetDeltaT(), 0.f, true);
		m_Transform->SetAngle(270.f);
		break;
	case Down:
		m_Transform->Move(.0f, speed * Time::GetInstance().GetDeltaT(), 0.f, true);
		m_Transform->SetAngle(90.f);
		break;
	case Left:
		m_Transform->Move(-speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true);
		m_Transform->SetAngle(180.f);
		break;
	case Right:
		m_Transform->Move(speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true);
		m_Transform->SetAngle(0.f);
		break;
	}
	
}
