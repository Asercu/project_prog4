#pragma once
#include "Singleton.h"

namespace dae
{
	class Scene;
	class SceneManager final : public Singleton<SceneManager>
	{
	public:
		Scene & CreateScene(const std::string& name);
		void AddScene(std::shared_ptr<Scene> pScene);
		void DeleteScene(const std::string& name);
		std::shared_ptr<Scene> GetActiveScene();

		bool HandleInput();
		void Update();
		void Render();

		void SetActiveScene(std::string SceneName);
	private:
		std::vector<std::shared_ptr<Scene>> m_Scenes;
		std::vector<std::string> m_SceneNames;
		size_t m_ActiveSceneIndex;

	};

}
