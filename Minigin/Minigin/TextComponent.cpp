#include "MiniginPCH.h"
#include "Renderer.h"
#include "Transform.h"
#include "ResourceManager.h"
#include "ComponentManager.h"

void dae::TextComponent::SetPivot(glm::vec3* pivot)
{
	m_pPivot = pivot;
}

dae::TextComponent::TextComponent(ComponentManager* pCompManager)
	:m_pFont(nullptr)
	,m_pTexture(nullptr)
	,m_MustUpdate(false)
	, m_pText{new std::string()}
	,m_Color{ 255,255,255 }
{
	pCompManager->AddRenderComponent(this);
}

void dae::TextComponent::SetText(std::string text)
{
	delete m_pText;
	m_pText = new std::string(text);
	m_MustUpdate = true;
}

void dae::TextComponent::SetFont(std::string font, unsigned int size)
{
	m_pFont = ResourceManager::GetInstance().LoadFont(font, size);
}

void dae::TextComponent::SetColor(SDL_Color color)
{
	m_Color = color;
}

void dae::TextComponent::Update()
{
	if (!m_MustUpdate) return;
	if (!m_pFont)
	{
		throw std::runtime_error(std::string("Render text failed: font was nullpointer"));
	}
	const auto surf = TTF_RenderText_Blended(m_pFont->GetFont(), m_pText->c_str(), m_Color);
	if (surf == nullptr)
	{
		throw std::runtime_error(std::string("Render text failed: ") + SDL_GetError());
	}
	auto texture = SDL_CreateTextureFromSurface(Renderer::GetInstance().GetSDLRenderer(), surf);
	if (texture == nullptr)
	{
		throw std::runtime_error(std::string("Create text texture from surface failed: ") + SDL_GetError());
	}
	SDL_FreeSurface(surf);
	delete m_pTexture;
	m_pTexture = new Texture2D(texture);
}

void dae::TextComponent::Render()
{
	if (m_pTexture)
	{
		Renderer::GetInstance().RenderText(this);
		//Renderer::GetInstance().RenderTexture(*m_pTexture, GetGameObject()->GetTransform()->GetPosition().x, GetGameObject()->GetTransform()->GetPosition().y);
	}
}

void dae::TextComponent::CleanUp()
{
	delete m_pText;
	delete m_pTexture;
}
