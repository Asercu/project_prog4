#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "Transform.h"

dae::GameObject::~GameObject()
{
	for(GameObject* pChild : m_pChildren)
	{
		delete pChild;
	}
	delete m_Transform;
	for (Component* c : m_pComponents)
	{
		c->CleanUp();
		delete c;
	}
};

void dae::GameObject::Update()
{
	for (Component* c : m_pComponents)
	{
		c->Update();
	}
}

void dae::GameObject::Render() const
{
	/*

	*/
}

dae::Component* dae::GameObject::AddComponent(Component* pComponent)
{
	m_pComponents.push_back(pComponent);
	pComponent->SetOwner(this);
	return pComponent;
}

dae::Scene* dae::GameObject::GetParentScene() const
{
	if (m_pParent)
		return m_pParent->GetParentScene();
	return m_pParentScene;
}

void dae::GameObject::SetParent(GameObject* parent)
{
	m_pParent = parent;
}

void dae::GameObject::SetParent(Scene* scene)
{
	m_pParentScene = scene;
}

void dae::GameObject::AddChild(GameObject* child)
{
	m_pChildren.push_back(child);
	child->SetParent(this);
	child->GetTransform()->Update();
	Scene* pS = GetParentScene();
	if (pS) pS->GetComponentManager()->SortRenderComponents();
}

void dae::GameObject::SetActive(bool enable)
{
	m_IsActive = enable;
	for (CommandInterface* pCmd : m_Commands)
	{
		pCmd->IsActive = enable;
	}
}

dae::GameObject::GameObject(Transform* transform)
	:m_pComponents{}
	,m_pChildren{}
	,m_pParent(nullptr)
{
	m_Transform = transform;
	m_Transform->SetOwner(this);
}

dae::GameObject::GameObject(SceneManagers* pManagers, std::string tag, bool threaded)
	:m_pComponents{}
	, m_pChildren{}
	,m_pParent(nullptr)
	,m_Tag(tag)
	,m_Threaded(threaded)
{
	m_Transform = new Transform(pManagers->pComp);
	m_Transform->SetOwner(this);
}



