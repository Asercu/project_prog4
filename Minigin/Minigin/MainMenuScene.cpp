#include "MiniginPCH.h"
#include "MainMenuScene.h"
#include "TextureComponent.h"
#include "Transform.h"
#include "TextComponent.h"
#include "PacmanScene.h"

dae::MainmenuScene::MainmenuScene(const std::string& SceneName) : Scene(SceneName)
{
	Scene::Init();
	Init();
}

void dae::MainmenuScene::Init()
{
	auto BG = new GameObject(m_pManagers);
	BG->AddComponent(new TextureComponent(m_pManagers->pComp));
	TextureComponent* texComp = BG->GetComponent<TextureComponent>();
	texComp->SetTexture("background.jpg");
	BG->GetTransform()->SetPosition(0.f, 0.f, -10.f);
	Add(BG);

	auto title = new GameObject(m_pManagers);
	TextComponent* pTextComp = static_cast<TextComponent*>(title->AddComponent(new TextComponent(m_pManagers->pComp)));
	pTextComp->SetText("Pacman");
	pTextComp->SetFont("Lingua.otf", 100);
	title->GetTransform()->SetPosition(120.f, 20.f, 0.f);
	Add(title);

	auto singleplayer = new GameObject(m_pManagers);
	m_pSingle = static_cast<TextComponent*>(singleplayer->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pSingle->SetText("1 player");
	m_pSingle->SetFont("Lingua.otf", 50);
	m_pSingle->SetColor({ 255, 0, 0, 255 });
	singleplayer->GetTransform()->SetPosition(200.f, 180.f, 0.f);
	Add(singleplayer);

	auto multiplayer = new GameObject(m_pManagers);
	m_pDouble = static_cast<TextComponent*>(multiplayer->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pDouble->SetText("2 players");
	m_pDouble->SetFont("Lingua.otf", 50);
	m_pDouble->SetColor({ 255, 255, 255, 255 });
	multiplayer->GetTransform()->SetPosition(200, 240.f, 0.f);
	Add(multiplayer);

	auto ghost = new GameObject(m_pManagers);
	m_pGhost = static_cast<TextComponent*>(ghost->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pGhost->SetText("2 players (ghost)");
	m_pGhost->SetFont("Lingua.otf", 50);
	m_pGhost->SetColor({ 255, 255, 255, 255 });
	ghost->GetTransform()->SetPosition(200, 300.f, 0.f);
	Add(ghost);

	auto exit = new GameObject(m_pManagers);
	m_pExit = static_cast<TextComponent*>(exit->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pExit->SetText("Quit Game");
	m_pExit->SetFont("Lingua.otf", 50);
	m_pExit->SetColor({ 255, 255, 255, 255 });
	exit->GetTransform()->SetPosition(200, 370.f, 0.f);
	Add(exit);

	std::function<void(const MainmenuScene&)> prev = std::bind(&MainmenuScene::Previous, this);
	Command<MainmenuScene>* cmdPrev = new Command<MainmenuScene>(this);
	cmdPrev->device = InputDevice::Controller;
	cmdPrev->controllerID = 0;
	cmdPrev->action = InputAction::Pressed;
	cmdPrev->btn = ControllerButton::DPadUP;
	cmdPrev->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrev);

	std::function<void(const MainmenuScene&)> next = std::bind(&MainmenuScene::Next, this);
	Command<MainmenuScene>* cmdNext = new Command<MainmenuScene>(this);
	cmdNext->device = InputDevice::Controller;
	cmdNext->controllerID = 0;
	cmdNext->action = InputAction::Pressed;
	cmdNext->btn = ControllerButton::DPadDown;
	cmdNext->fn = next;
	m_pManagers->pInput->AddAction(cmdNext);

	std::function<void(const MainmenuScene&)> select = std::bind(&MainmenuScene::Select, this);
	Command<MainmenuScene>* cmdSelect = new Command<MainmenuScene>(this);
	cmdSelect->device = InputDevice::Controller;
	cmdSelect->controllerID = 0;
	cmdSelect->action = InputAction::Pressed;
	cmdSelect->btn = ControllerButton::ButtonA;
	cmdSelect->fn = select;
	m_pManagers->pInput->AddAction(cmdSelect);

	//std::function<void(const MainmenuScene&)> prev = std::bind(&MainmenuScene::Previous, this);
	Command<MainmenuScene>* cmdPrevKB = new Command<MainmenuScene>(this);
	cmdPrevKB->device = InputDevice::KeyBoard;
	cmdPrevKB->action = InputAction::Pressed;
	cmdPrevKB->key = SDL_SCANCODE_UP;
	cmdPrevKB->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrevKB);

	//std::function<void(const MainmenuScene&)> next = std::bind(&MainmenuScene::Next, this);
	Command<MainmenuScene>* cmdNextKB = new Command<MainmenuScene>(this);
	cmdNextKB->device = InputDevice::KeyBoard;
	cmdNextKB->action = InputAction::Pressed;
	cmdNextKB->key = SDL_SCANCODE_DOWN;
	cmdNextKB->fn = next;
	m_pManagers->pInput->AddAction(cmdNextKB);

	//std::function<void(const MainmenuScene&)> select = std::bind(&MainmenuScene::Select, this);
	Command<MainmenuScene>* cmdSelectKB = new Command<MainmenuScene>(this);
	cmdSelectKB->device = InputDevice::KeyBoard;
	cmdSelectKB->action = InputAction::Pressed;
	cmdSelectKB->key = SDL_SCANCODE_RETURN;
	cmdSelectKB->fn = select;
	m_pManagers->pInput->AddAction(cmdSelectKB);
}

void dae::MainmenuScene::Update()
{
	Scene::Update();
}

void dae::MainmenuScene::Previous()
{
	if (--selectedIndex < 0) selectedIndex = 0;
	SetColors();
}

void dae::MainmenuScene::Next()
{
	if (++selectedIndex > 3) selectedIndex = 3;
	SetColors();
}

void dae::MainmenuScene::Select()
{
	switch (selectedIndex)
	{
	case 0:
		{
			std::shared_ptr<Scene> single(new PacmanScene("Level1.pac"));
			SceneManager::GetInstance().AddScene(single);
			SceneManager::GetInstance().SetActiveScene("Level1.pac");
		}

		break;
	case 1:
		{
			std::shared_ptr<Scene> single(new PacmanScene("Level2.pac"));
			SceneManager::GetInstance().AddScene(single);
			SceneManager::GetInstance().SetActiveScene("Level2.pac");
		}
		break;
	case 2:
		{
		std::shared_ptr<Scene> single(new PacmanScene("Level3.pac"));
		SceneManager::GetInstance().AddScene(single);
		SceneManager::GetInstance().SetActiveScene("Level3.pac");
		}
		break;
	case 3:
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
		break;
	}
}

void dae::MainmenuScene::SetColors()
{
	SDL_Color red = { 255,0,0,255 };
	SDL_Color white = { 255,255,255,255 };

	m_pSingle->SetColor(selectedIndex == 0 ? red : white);
	m_pDouble->SetColor(selectedIndex == 1 ? red : white);
	m_pGhost->SetColor(selectedIndex == 2 ? red : white);
	m_pExit->SetColor(selectedIndex == 3 ? red : white);
}

