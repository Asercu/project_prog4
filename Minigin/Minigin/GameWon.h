#pragma once
#include "Scene.h"

namespace dae
{
	class TextComponent;

	class GameWonScene : public Scene
	{
	public:
		GameWonScene(const std::string& SceneName);
		void Init() override;
		void Update() override;

		void Previous();
		void Next();
		void Select();

	private:
		int selectedIndex = 0;
		TextComponent* m_pRetry;
		TextComponent* m_pMainMenu;
		TextComponent* m_pExit;

		void SetColors();
	};
}