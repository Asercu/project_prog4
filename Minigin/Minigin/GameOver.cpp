#include "MiniginPCH.h"
#include "GameOver.h"
#include "TextureComponent.h"
#include "Transform.h"
#include "TextComponent.h"
#include "DataVault.h"
#include "PacmanScene.h"

dae::GameOverScene::GameOverScene(const std::string& SceneName) : Scene(SceneName)
{
	Scene::Init();
	Init();
}

void dae::GameOverScene::Init()
{
	auto BG = new GameObject(m_pManagers);
	BG->AddComponent(new TextureComponent(m_pManagers->pComp));
	TextureComponent* texComp = BG->GetComponent<TextureComponent>();
	texComp->SetTexture("background.jpg");
	BG->GetTransform()->SetPosition(0.f, 0.f, -10.f);
	Add(BG);

	auto title = new GameObject(m_pManagers);
	TextComponent* pTextComp = static_cast<TextComponent*>(title->AddComponent(new TextComponent(m_pManagers->pComp)));
	pTextComp->SetText("Game Over");
	pTextComp->SetFont("Lingua.otf", 100);
	title->GetTransform()->SetPosition(50.f, 20.f, 0.f);
	Add(title);

	auto retry = new GameObject(m_pManagers);
	m_pRetry = static_cast<TextComponent*>(retry->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pRetry->SetText("Retry");
	m_pRetry->SetFont("Lingua.otf", 50);
	m_pRetry->SetColor({ 255, 0, 0, 255 });
	retry->GetTransform()->SetPosition(200.f, 240.f, 0.f);
	Add(retry);

	auto mainmenu = new GameObject(m_pManagers);
	m_pMainMenu = static_cast<TextComponent*>(mainmenu->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pMainMenu->SetText("Main Menu");
	m_pMainMenu->SetFont("Lingua.otf", 50);
	m_pMainMenu->SetColor({ 255, 255, 255, 255 });
	mainmenu->GetTransform()->SetPosition(200.f, 290.f, 0.f);
	Add(mainmenu);

	auto exit = new GameObject(m_pManagers);
	m_pExit = static_cast<TextComponent*>(exit->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pExit->SetText("Quit Game");
	m_pExit->SetFont("Lingua.otf", 50);
	m_pExit->SetColor({ 255, 255, 255, 255 });
	exit->GetTransform()->SetPosition(200, 350.f, 0.f);
	Add(exit);

	auto scoreP1 = new GameObject(m_pManagers);
	TextComponent* score1 = static_cast<TextComponent*>(scoreP1->AddComponent(new TextComponent(m_pManagers->pComp)));
	score1->SetFont("Lingua.otf", 50);
	score1->SetText("P1: " + std::to_string(DataVault::GetInstance().scoreP1));
	score1->SetColor({ 255,255,255,255 });
	scoreP1->GetTransform()->SetPosition(120.f, 150.f, 1.f);
	Add(scoreP1);

	if (DataVault::GetInstance().scoreP2 != -1)
	{
		auto scoreP2 = new GameObject(m_pManagers);
		TextComponent* score2 = static_cast<TextComponent*>(scoreP2->AddComponent(new TextComponent(m_pManagers->pComp)));
		score2->SetFont("Lingua.otf", 50);
		score2->SetText("P2: " + std::to_string(DataVault::GetInstance().scoreP2));
		score2->SetColor({ 255,255,255,255 });
		scoreP2->GetTransform()->SetPosition(380.f, 150, 1.f);
		Add(scoreP2);
	}
	else
	{
		scoreP1->GetTransform()->SetPosition(250.f, 150.f, 1.f);
	}
	

	std::function<void(const GameOverScene&)> prev = std::bind(&GameOverScene::Previous, this);
	Command<GameOverScene>* cmdPrev = new Command<GameOverScene>(this);
	cmdPrev->device = InputDevice::Controller;
	cmdPrev->controllerID = 0;
	cmdPrev->action = InputAction::Pressed;
	cmdPrev->btn = ControllerButton::DPadUP;
	cmdPrev->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrev);

	std::function<void(const GameOverScene&)> next = std::bind(&GameOverScene::Next, this);
	Command<GameOverScene>* cmdNext = new Command<GameOverScene>(this);
	cmdNext->device = InputDevice::Controller;
	cmdNext->controllerID = 0;
	cmdNext->action = InputAction::Pressed;
	cmdNext->btn = ControllerButton::DPadDown;
	cmdNext->fn = next;
	m_pManagers->pInput->AddAction(cmdNext);

	std::function<void(const GameOverScene&)> select = std::bind(&GameOverScene::Select, this);
	Command<GameOverScene>* cmdSelect = new Command<GameOverScene>(this);
	cmdSelect->device = InputDevice::Controller;
	cmdSelect->controllerID = 0;
	cmdSelect->action = InputAction::Pressed;
	cmdSelect->btn = ControllerButton::ButtonA;
	cmdSelect->fn = select;
	m_pManagers->pInput->AddAction(cmdSelect);

	//std::function<void(const MainmenuScene&)> prev = std::bind(&MainmenuScene::Previous, this);
	Command<GameOverScene>* cmdPrevKB = new Command<GameOverScene>(this);
	cmdPrevKB->device = InputDevice::KeyBoard;
	cmdPrevKB->action = InputAction::Pressed;
	cmdPrevKB->key = SDL_SCANCODE_UP;
	cmdPrevKB->fn = prev;
	m_pManagers->pInput->AddAction(cmdPrevKB);

	//std::function<void(const MainmenuScene&)> next = std::bind(&MainmenuScene::Next, this);
	Command<GameOverScene>* cmdNextKB = new Command<GameOverScene>(this);
	cmdNextKB->device = InputDevice::KeyBoard;
	cmdNextKB->action = InputAction::Pressed;
	cmdNextKB->key = SDL_SCANCODE_DOWN;
	cmdNextKB->fn = next;
	m_pManagers->pInput->AddAction(cmdNextKB);

	//std::function<void(const MainmenuScene&)> select = std::bind(&MainmenuScene::Select, this);
	Command<GameOverScene>* cmdSelectKB = new Command<GameOverScene>(this);
	cmdSelectKB->device = InputDevice::KeyBoard;
	cmdSelectKB->action = InputAction::Pressed;
	cmdSelectKB->key = SDL_SCANCODE_RETURN;
	cmdSelectKB->fn = select;
	m_pManagers->pInput->AddAction(cmdSelectKB);
}

void dae::GameOverScene::Update()
{
	Scene::Update();
}

void dae::GameOverScene::Previous()
{
	if (--selectedIndex < 0) selectedIndex = 0;
	SetColors();
}

void dae::GameOverScene::Next()
{
	if (++selectedIndex > 2) selectedIndex = 2;
	SetColors();
}

void dae::GameOverScene::Select()
{
	switch (selectedIndex)
	{
	case 0:
		{
		SceneManager::GetInstance().DeleteScene(DataVault::GetInstance().pausedScene);
		std::shared_ptr<Scene> scene(new PacmanScene(DataVault::GetInstance().pausedScene));
		SceneManager::GetInstance().AddScene(scene);
		SceneManager::GetInstance().SetActiveScene(DataVault::GetInstance().pausedScene);
		}
		break;
	case 1:
		SceneManager::GetInstance().DeleteScene(DataVault::GetInstance().pausedScene);
		SceneManager::GetInstance().SetActiveScene("main");
		break;
	case 2:
		SDL_Event quitEvent;
		quitEvent.type = SDL_QUIT;
		SDL_PushEvent(&quitEvent);
		break;
	}
}

void dae::GameOverScene::SetColors()
{
	SDL_Color red = { 255,0,0,255 };
	SDL_Color white = { 255,255,255,255 };

	m_pRetry->SetColor(selectedIndex == 0 ? red : white);
	m_pMainMenu->SetColor(selectedIndex == 1 ? red : white);
	m_pExit->SetColor(selectedIndex == 2 ? red : white);
}

