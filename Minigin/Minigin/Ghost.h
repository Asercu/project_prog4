#pragma once
#include "GameObject.h"
#include "TextureComponent.h"


namespace dae
{
	class Ghost : public GameObject
	{
	public:
		enum Direction
		{
			NONE,
			Up,
			Down,
			Left,
			Right
		};

		enum State
		{
			none,
			Killable,
			Dead
		};
		enum Colors
		{
			Red,
			Orange,
			Pink,
			Blue
		};
		Ghost(SceneManagers* pManagers, Colors c);
		void MegaPelletCollected();
		void Move();

		void Update() override;
	private:
		TextureComponent* m_pTexComp;
		CollisionComponent* m_pCollider;
		State m_State = none;
		float m_Timer = 0.f;
		Colors m_Color;
		void SetTexture();
		float speed = 130.f;
		Direction m_CurrentDir;

		void MoveDown();
		void MoveUp();
		void MoveLeft();
		void MoveRight();
	};
}
