#pragma once
#include <XInput.h>
#include "Singleton.h"
#include <functional>
#include "GameObject.h"
#include <SDL.h>
#include "Structs.h"

namespace dae
{
	enum class InputDevice
	{
		Controller,
		KeyBoard,
		AutoExecute
	};

	enum class ControllerButton
	{
		UNSET,
		ButtonA,
		ButtonB,
		ButtonX,
		ButtonY,
		ButtonLB,
		ButtonRB,
		ButtonBACK,
		ButtonSTART,
		ButtonLSTICK,
		ButtonRSTICK,
		DPadUP,
		DPadDown,
		DPadRight,
		DPadLeft,
		TriggerRight,
		TriggerLeft,
	};

	enum class InputAction
	{
		Pressed,
		Down,
		Released
	};

	class CommandInterface
	{
	public:
		bool IsActive = true;
		bool Threaded = false;
		InputDevice device;
		unsigned int controllerID;
		ControllerButton btn;
		SDL_Scancode key;
		InputAction action = InputAction::Pressed;
		virtual void Execute() {};
		bool executed = true;
	};

	template<typename objectType>
	class Command : public CommandInterface
	{
	public:
		Command<objectType>(objectType* actor) : Actor(actor) {};
		objectType* Actor;
		std::function<void(const objectType&)> fn;
		virtual void Execute() override
		{
			fn(*Actor);
		}
	};

	class InputManager
	{
	public:
		bool ProcessInput();
		bool IsDown(XINPUT_STATE state, ControllerButton button) const;
		void AddAction(CommandInterface* cmd);
		float2 GetLeftStickPos(unsigned int controllerID);
		float2 GetRightStickPos(unsigned int controllerID);
		float GetLeftTrigger(unsigned int controllerID);
		float GetRightTrigger(unsigned int controllerID);

		InputManager();
		~InputManager();
	private:
		std::vector<XINPUT_STATE> m_CurrentControllerStates{};
		std::vector<XINPUT_STATE> m_LastControllerStates{};
		std::vector<CommandInterface*> m_Actions;
		std::vector<CommandInterface*> m_ThreadedActions;
		const Uint8* m_KeyboardState = SDL_GetKeyboardState(nullptr);
		void HandleCmd(CommandInterface* cmd);
	};
}
