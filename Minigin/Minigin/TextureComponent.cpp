#include "MiniginPCH.h"
#include "TextureComponent.h"
#include <SDL.h>
#include "Transform.h"
#include "Renderer.h"
#include "ResourceManager.h"
#include "ComponentManager.h"


void dae::TextureComponent::Render()
{
	if (m_pTex)
	{
		Renderer::GetInstance().RenderTexture(this);
		//Renderer::GetInstance().RenderTexture(*m_pTex, GetGameObject()->GetTransform()->GetPosition().x, GetGameObject()->GetTransform()->GetPosition().y);
	}
}

void dae::TextureComponent::SetPivot(glm::vec3* pivot)
{
	m_pPivot = pivot;
}

void dae::TextureComponent::SetTexture(std::string file)
{
	m_pTex = ResourceManager::GetInstance().LoadTexture(file);
}

dae::TextureComponent::TextureComponent(ComponentManager* pCompManager)
	:m_pTex(nullptr)
	,m_pPivot(nullptr)
{
	pCompManager->AddRenderComponent(this);
}
