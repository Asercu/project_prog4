#include "MiniginPCH.h"
#include "ComponentManager.h"
#include "Component.h"
#include <algorithm>
#include "Transform.h"

void dae::ComponentManager::AddComponent(Component* pComp)
{
	m_pComponents.push_back(pComp);
}

void dae::ComponentManager::AddRenderComponent(Component* pComp)
{
	m_pRenderComponents.push_back(pComp);
}

void dae::ComponentManager::SortRenderComponents()
{
	std::sort(m_pRenderComponents.begin(), m_pRenderComponents.end(), [](Component* A, Component* B)
	{
		Transform* Atrans = A->GetGameObject()->GetTransform();
		Transform* Btrams = B->GetGameObject()->GetTransform();
		int i = 5;
		if (Atrans < Btrams) i++;
		return A->GetGameObject()->GetTransform()->GetWorldPosition().z < B->GetGameObject()->GetTransform()->GetWorldPosition().z;
	});
}

dae::ComponentManager::ComponentManager()
	:m_pRenderComponents{}
	,m_pComponents{}
{}
