#pragma once
#pragma warning(push)
#pragma warning (disable:4201)
#include <glm/vec3.hpp>
#include "Component.h"
#pragma warning(pop)

namespace dae
{
	class ComponentManager;

	class Transform : public Component
	{
	private:
		glm::vec3 m_Position;
		float m_Angle;
		glm::vec3 m_Scale;
		glm::vec3 m_WorldPosition;
		float m_WorldAngle;
		glm::vec3 m_WorldScale;

		void UpdateChildren();

	public:
		void SetPosition(float x, float y, float z);
		void SetPosition(glm::vec3 pos);
		void SetAngle(float angle);
		void SetScale(float x, float y, float z);
		void SetScale(glm::vec3 scale);
		glm::vec3 GetPosition() const;
		float GetAngle() const;
		glm::vec3 GetScale() const;
		glm::vec3 GetWorldPosition() const;
		float GetWorldAngle() const;
		glm::vec3 GetWorldScale() const;
		void Move(float x, float y, float z, bool collision = false, std::string ignoreTag = "NOTSET");

		Transform(ComponentManager* pCompManager);
		void Update(Transform* parentTransform = nullptr);
	};
}
