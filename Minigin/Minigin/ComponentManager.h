#pragma once
#include "Singleton.h"
#include <vector>


namespace dae
{
	class Component;
	class ComponentManager
	{
	private:
		std::vector<Component*> m_pRenderComponents{};
		std::vector<Component*> m_pComponents{};
	public:
		void AddComponent(Component* pComp);
		void AddRenderComponent(Component* pComp);
		std::vector<Component*>& GetRenderComponents() { return m_pRenderComponents; };
		std::vector<Component*>& GetComponents() { return m_pComponents; };
		void SortRenderComponents();
		ComponentManager();
	};
}
