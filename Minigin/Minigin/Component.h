#pragma once
#include "GameObject.h"

namespace dae
{
	class Component
	{
	public:
		void SetOwner(GameObject* owner) { m_pOwner = owner; };
		GameObject* GetGameObject() const { return m_pOwner; };
		virtual void Update() {};
		virtual void Render() {};
		virtual void CleanUp() {};
	private:
		GameObject* m_pOwner;
	};
}
