#include "MiniginPCH.h"
#include "MegaPellet.h"
#include "CollisionComponent.h"
#include "TextureComponent.h"
#include "Time.h"
#include "PacmanScene.h"

dae::MegaPellet::MegaPellet(SceneManagers* pManagers) : GameObject(pManagers)
{
	m_pCollider = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape CS;
	SDL_Rect box;
	box.x = 6;
	box.y = 6;
	box.h = 20;
	box.w = 20;
	CS.box = box;
	CS.dynamic = false;
	CS.type = Trigger;
	m_pCollider->SetShape(CS);
	m_pTexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	m_pTexComp->SetTexture("megaPelletYellow.png");
}

void dae::MegaPellet::Update()
{
	GameObject::Update();

	auto collisions = m_pCollider->GetOverLappingObjects();
	for (GameObject* pG : *collisions)
	{
		if (pG->GetTag() == "Pacman")
		{
			SetActive(false);
			Scene* sc = GetParentScene();
			PacmanScene* s = static_cast<PacmanScene*>(sc);
			s->MegaPelletCollected();
		}
	}
	delete collisions;

	m_Timer += Time::GetInstance().GetDeltaT();
	if (m_Timer >= m_ColorTime)
	{
		m_Timer = 0.f;
		m_IsYellow = !m_IsYellow;
		m_pTexComp->SetTexture(m_IsYellow ? "megaPelletYellow.png" : "megaPelletWhite.png");
	}
}
