#include "MiniginPCH.h"
#include "SceneManager.h"
#include "Scene.h"


bool dae::SceneManager::HandleInput()
{
	return m_Scenes[m_ActiveSceneIndex]->HandleInput();
}

void dae::SceneManager::Update()
{
	/*
	for(auto scene : mScenes)
	{
		scene->Update();
	}
	*/
	m_Scenes[m_ActiveSceneIndex]->Update();
}

void dae::SceneManager::Render()
{
	/*
	for (const auto scene : m_Scenes)
	{
		scene->Render();
	}
	*/
	m_Scenes[m_ActiveSceneIndex]->Render();
}

void dae::SceneManager::SetActiveScene(std::string SceneName)
{
	for (size_t i = 0; i < m_SceneNames.size(); ++i)
	{
		if (m_SceneNames[i] == SceneName)
		{
			m_ActiveSceneIndex = i;
			return;
		}
	}
	std::cout << "SceneManager::SetActiveScene > Error Activating Scene " << SceneName << ": Scene not found" << std::endl;
}

dae::Scene& dae::SceneManager::CreateScene(const std::string& name)
{
	const auto scene = std::shared_ptr<Scene>(new Scene(name));
	m_Scenes.push_back(scene);
	m_SceneNames.push_back(name);
	m_ActiveSceneIndex = m_Scenes.size() - 1;
	return *scene;
}

void dae::SceneManager::AddScene(std::shared_ptr<Scene> pScene)
{
	m_Scenes.push_back(pScene);
	m_SceneNames.push_back(pScene.get()->GetName());
	m_ActiveSceneIndex = m_Scenes.size() - 1;
}

void dae::SceneManager::DeleteScene(const std::string& name)
{
	int index = -1;
	for (unsigned int i = 0; i < m_SceneNames.size(); ++i)
	{
		if (m_SceneNames[i] == name)
		{
			index = int(i);
		}
	}
	if (index < 0) return;
	m_SceneNames.erase(m_SceneNames.begin() + index);
	m_Scenes.erase(m_Scenes.begin() + index);
}

std::shared_ptr<dae::Scene> dae::SceneManager::GetActiveScene()
{
	return m_Scenes[m_ActiveSceneIndex];
}
