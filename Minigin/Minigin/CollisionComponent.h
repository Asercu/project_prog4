#pragma once
#include "Component.h"


namespace dae
{
	struct ColliderShape
	{
		bool dynamic;
		SDL_Rect box;
		ColliderTypes type = Collider;
	};

	class ComponentManager;

	class CollisionComponent : public Component
	{
	public:
		CollisionComponent(SceneManagers* pManagers);
		void SetShape(ColliderShape CS);
		void Add();
		std::vector<GameObject*>* GetOverLappingObjects();
		void Render() override;
		bool GetDynamic() { return m_dynamic || !m_Added; }
		void UpdateBox();
		CollisionManager* GetCollisionManager() { return pCollisionManager; };
		ColliderTypes getType() { return m_Type; };
		float2 getColliderCenter() { return m_ColliderCenter; };

	private:
		friend class CollisionManager;
		bool m_dynamic = true;
		SDL_Rect box;
		SDL_Rect localBox;
		ColliderTypes m_Type = Collider;
		CollisionManager* pCollisionManager;
		float2 m_ColliderCenter;
		bool m_Added = false;
	};
}
