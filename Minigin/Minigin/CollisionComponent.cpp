#include "MiniginPCH.h"
#include "CollisionComponent.h"
#include "Renderer.h"
#include "Transform.h"

dae::CollisionComponent::CollisionComponent(SceneManagers* pManagers)
{
	pManagers->pComp->AddComponent(this);
	pCollisionManager = pManagers->pCollision;
}

void dae::CollisionComponent::SetShape(ColliderShape CS)
{
	localBox = CS.box;
	UpdateBox();
	m_dynamic = CS.dynamic;
	if (CS.type == Both)
	{
		std::cout << "WARNING: CollisionComponent::SetShape > Type Both not allowed for collider, using collision instead" << std::endl;
		m_Type = Collider;
	}
	else
	{
		m_Type = CS.type;
	}
	//Add();
}

void dae::CollisionComponent::Add()
{
	m_Added = true;
	pCollisionManager->AddCollider(this);
}

std::vector<dae::GameObject*>* dae::CollisionComponent::GetOverLappingObjects()
{
	return pCollisionManager->GetOverLaps(this);
}

void dae::CollisionComponent::Render()
{
	SDL_SetRenderDrawColor(Renderer::GetInstance().GetSDLRenderer(), 255, 0, 0, 255);
	SDL_RenderDrawRect(Renderer::GetInstance().GetSDLRenderer(), &box);
	SDL_SetRenderDrawColor(Renderer::GetInstance().GetSDLRenderer(), 0, 0, 0, 255);
}

void dae::CollisionComponent::UpdateBox()
{
	box.x = int(GetGameObject()->GetTransform()->GetWorldPosition().x) + localBox.x;
	box.y = int(GetGameObject()->GetTransform()->GetWorldPosition().y) + localBox.y;
	box.w = localBox.w;
	box.h = localBox.h;
	m_ColliderCenter = { box.x + box.w * 0.5f, box.y + box.h * 0.5f };
}
