#include "MiniginPCH.h"
#include "PacmanScene.h"
#include "BinaryReader.h"
#include "Block.h"
#include "Transform.h"
#include "Ghost.h"
#include "Pacman.h"
#include "MegaPellet.h"
#include "Pellet.h"
#include "TextComponent.h"
#include "DataVault.h"
#include "MainMenuScene.h"
#include "GameOver.h"
#include "GameWon.h"
#include "PlayerGhost.h"

dae::PacmanScene::PacmanScene(const std::string& levelName) : Scene(levelName)
{
	m_levelName = levelName;
	Init();
}

void dae::PacmanScene::Init()
{
	Scene::Init();
	LoadLevelFromFile();

	DataVault::GetInstance().scoreP1 = 0;
	DataVault::GetInstance().scoreP2 = -1;

	std::function<void(const PacmanScene&)> pause = std::bind(&PacmanScene::Pause, this);
	Command<PacmanScene>* cmdPauseKB = new Command<PacmanScene>(this);
	cmdPauseKB->device = InputDevice::KeyBoard;
	cmdPauseKB->action = InputAction::Pressed;
	cmdPauseKB->key = SDL_SCANCODE_ESCAPE;
	cmdPauseKB->fn = pause;
	m_pManagers->pInput->AddAction(cmdPauseKB);

	//player one UI
	GameObject* score1 = new GameObject(m_pManagers);
	score1->GetTransform()->SetPosition(10.f, 0.f, 3.f);
	m_pScoreTextP1 = static_cast<TextComponent*>(score1->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pScoreTextP1->SetText("0");
	m_pScoreTextP1->SetFont("Lingua.otf", 36);
	Add(score1);

	for (int i = 0; i < 3; ++i)
	{
		m_LiveObjectsP1[i] = new GameObject(m_pManagers);
		m_LiveObjectsP1[i]->GetTransform()->SetPosition(200.f - 32.f * (i+1),3.2f, 10.f);
		m_LiveObjectsP1[i]->GetTransform()->SetScale(.8f, .8f, 1.f);
		TextureComponent* pTexComp = static_cast<TextureComponent*>(m_LiveObjectsP1[i]->AddComponent(new TextureComponent(m_pManagers->pComp)));
		pTexComp->SetTexture("Pacman.gif");
		Add(m_LiveObjectsP1[i]);
	}
	
	//std::function<void(const PacmanScene&)> pause = std::bind(&PacmanScene::Pause, this);
	Command<PacmanScene>* cmdPause = new Command<PacmanScene>(this);
	cmdPause->device = InputDevice::Controller;
	cmdPause->controllerID = 0;
	cmdPause->action = InputAction::Pressed;
	cmdPause->btn = ControllerButton::ButtonSTART;
	cmdPause->fn = pause;
	m_pManagers->pInput->AddAction(cmdPause);

	//player two UI
	if (m_Player2 == nullptr || dynamic_cast<Pacman*>(m_Player2) == nullptr) return;
	GameObject* score2 = new GameObject(m_pManagers);
	score2->GetTransform()->SetPosition(544.f, 0.f, 3.f);
	m_pScoreTextP2 = static_cast<TextComponent*>(score2->AddComponent(new TextComponent(m_pManagers->pComp)));
	m_pScoreTextP2->SetText("0");
	m_pScoreTextP2->SetFont("Lingua.otf", 36);
	Add(score2);

	for (int i = 0; i < 3; ++i)
	{
		m_LiveObjectsP2[i] = new GameObject(m_pManagers);
		m_LiveObjectsP2[i]->GetTransform()->SetPosition(544.f - 32.f * (i + 1), 3.2f, 10.f);
		m_LiveObjectsP2[i]->GetTransform()->SetScale(.8f, .8f, 1.f);
		TextureComponent* pTexComp = static_cast<TextureComponent*>(m_LiveObjectsP2[i]->AddComponent(new TextureComponent(m_pManagers->pComp)));
		pTexComp->SetTexture("mspacman.png");
		Add(m_LiveObjectsP2[i]);
	}

	Command<PacmanScene>* cmdPause2 = new Command<PacmanScene>(this);
	cmdPause2->device = InputDevice::Controller;
	cmdPause2->controllerID = 1;
	cmdPause2->action = InputAction::Pressed;
	cmdPause2->btn = ControllerButton::ButtonSTART;
	cmdPause2->fn = pause;
	m_pManagers->pInput->AddAction(cmdPause2);
}

void dae::PacmanScene::Update()
{
	Scene::Update();
	if (m_Player1 != nullptr)
	m_pScoreTextP1->SetText(std::to_string(m_ScoreP1));
	if (m_Player2 != nullptr && dynamic_cast<Pacman*>(m_Player2))
	m_pScoreTextP2->SetText(std::to_string(m_ScoreP2));
}

void dae::PacmanScene::AddScore(int points, GameObject* p)
{
	if (p == m_Player1)
	{
		m_ScoreP1 += points;
	}
	else if (p == m_Player2)
	{
		m_ScoreP2 += points;
	}
}

void dae::PacmanScene::PelletCollected(GameObject* pObj)
{
	if (pObj == m_Player1)
	{
		m_ScoreP1 += 10;
	}
	else if (pObj == m_Player2)
	{
		m_ScoreP2 += 10;
	}
	if (--m_PelletCount <= 0)
	{
		DataVault::GetInstance().pausedScene = m_Name;
		DataVault::GetInstance().scoreP1 = m_ScoreP1;
		if (m_Player2)
			DataVault::GetInstance().scoreP2 = m_ScoreP2;
		SceneManager::GetInstance().DeleteScene("gameWon");
		std::shared_ptr<Scene> gameOver(new GameWonScene("gameWon"));
		SceneManager::GetInstance().AddScene(gameOver);
	}
}

void dae::PacmanScene::MegaPelletCollected()
{
	m_ScoreP1 += 50;
	if (--m_PelletCount <= 0)
	{
		DataVault::GetInstance().pausedScene = m_Name;
		DataVault::GetInstance().scoreP1 = m_ScoreP1;
		if (m_Player2 && dynamic_cast<Pacman*>(m_Player2) != nullptr)
			DataVault::GetInstance().scoreP2 = m_ScoreP2;
		SceneManager::GetInstance().DeleteScene("gameWon");
		std::shared_ptr<Scene> gameOver(new GameWonScene("gameWon"));
		SceneManager::GetInstance().AddScene(gameOver);
	}
	if (m_Blue)	m_Blue->MegaPelletCollected();
	if (m_Red) m_Red->MegaPelletCollected();
	if (m_Orange) m_Orange->MegaPelletCollected();
	if (m_Pink) m_Pink->MegaPelletCollected();
}

void dae::PacmanScene::KillPacman(GameObject* pacman)
{
	if (pacman == m_Player1)
	{
		m_LiveObjectsP1[--m_LivesP1]->SetActive(false);
		if (m_LivesP1 > 0)
		{
			m_Player1->GetTransform()->SetPosition(m_Player1Start.x, m_Player1Start.y, 5.f);
		}
		else
		{
			m_Player1->SetActive(false);
			m_Player1->GetTransform()->SetPosition(-500.f, -500.f, 0.f);
		}
	}
	if (pacman == m_Player2)
	{
		m_LiveObjectsP2[--m_LivesP2]->SetActive(false);
		if (m_LivesP2 > 0)
		{
			m_Player2->GetTransform()->SetPosition(m_Player2Start.x, m_Player2Start.y, 5.f);
		}
		else
		{
			m_Player2->SetActive(false);
			m_Player2->GetTransform()->SetPosition(-500.f, -500.f, 0.f);
		}
	}
	if ((m_Player1 == nullptr || !m_Player1->IsActive()) && (m_Player2 == nullptr || !m_Player2->IsActive()))
	{
		DataVault::GetInstance().pausedScene = m_Name;
		DataVault::GetInstance().scoreP1 = m_ScoreP1;
		if (m_Player2 && dynamic_cast<Pacman*>(m_Player2))
			DataVault::GetInstance().scoreP2 = m_ScoreP2;
		SceneManager::GetInstance().DeleteScene("gameOver");
		std::shared_ptr<Scene> gameOver(new GameOverScene("gameOver"));
		SceneManager::GetInstance().AddScene(gameOver);
	}
}

void dae::PacmanScene::Pause()
{
	DataVault::GetInstance().pausedScene = m_Name;
	SceneManager::GetInstance().SetActiveScene("pause");
}

void dae::PacmanScene::LoadLevelFromFile()
{
	std::string name = "../Data/Levels/" + m_levelName;
	BinaryReader reader(name);
	reader.open();
	float x = 0.f;
	float y = 0.f;
	char c;
	bool fileEnd = false;
	do
	{
		c = reader.Read<char>();
		switch (c)
		{
		case 'X':
		{
			auto block = new Block(m_pManagers);
			block->GetTransform()->SetPosition(x, y, -1.f);
			x += 32.f;
			Add(block);
		}
		break;
		case '\n':
			y += 32.f;
			x = 0.f;
			break;
		case 'M':
		{
			auto pellet = new MegaPellet(m_pManagers);
			pellet->GetTransform()->SetPosition(x, y, 0.f);
			x += 32.f;
			m_PelletCount++;
			Add(pellet);
		}
		break;
		case 'P':
		{
			m_Pink = new Ghost(m_pManagers, Ghost::Pink);
			m_Pink->GetTransform()->SetPosition(x, y, 3.f);
			Add(m_Pink);
			x += 32.f;
		}
		break;
		case 'R':
		{
			m_Red = new Ghost(m_pManagers, Ghost::Red);
			m_Red->GetTransform()->SetPosition(x, y, 3.f);
			Add(m_Red);
			x += 32.f;
		}
		break;
		case 'O':
		{
			m_Orange = new Ghost(m_pManagers, Ghost::Orange);
			m_Orange->GetTransform()->SetPosition(x, y, 3.f);
			Add(m_Orange);
			x += 32.f;
		}
		break;
		case 'B':
		{
			m_Blue = new Ghost(m_pManagers, Ghost::Blue);
			m_Blue->GetTransform()->SetPosition(x, y, 3.f);
			Add(m_Blue);
			x += 32.f;
		}
		break;
		case '1':
		{
			m_Player1 = new Pacman(m_pManagers, 0);
			m_Player1->GetTransform()->SetPosition(x, y, 5.f);
			m_Player1Start = { x, y };
			Add(m_Player1);
			x += 32.f;
		}
		break;
		case '2':
		{
			m_Player2 = new Pacman(m_pManagers, 1);
			m_Player2->GetTransform()->SetPosition(x, y, 5.f);
			m_Player2Start = { x, y };
			Add(m_Player2);
			x += 32.f;
		}
		break;
		case '3':
			{
			m_Player2 = new PlayerGhost(m_pManagers);
			m_Player2->GetTransform()->SetPosition(x, y, 5.f);
			m_Player2Start = { x, y };
			Add(m_Player2);
			x += 32.f;
			}
			break;
		case 'C':
		{
			auto pellet = new Pellet(m_pManagers);
			pellet->GetTransform()->SetPosition(x, y, 5.f);
			Add(pellet);
			m_PelletCount++;
			x += 32.f;
		}
		break;
		case 'F':
			x += 32.f;
			break;
		case ' ':
			x += 32.f;
			break;
		case '$':
			fileEnd = true;
			break;
		}

	} while (!fileEnd);
}
