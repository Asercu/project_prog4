#include "MiniginPCH.h"
#include "ResourceManager.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "Renderer.h"
#include "Texture2D.h"
#include "Font.h"

dae::ResourceManager::ResourceManager()
	:m_TextureMap{}
	,m_FontMap{}
{
}

void dae::ResourceManager::Init(std::string&& dataPath)
{
	mDataPath = std::move(dataPath);

	// load support for png and jpg, this takes a while!

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) 
	{
		throw std::runtime_error(std::string("Failed to load support for png's: ") + SDL_GetError());
	}

	if ((IMG_Init(IMG_INIT_JPG) & IMG_INIT_JPG) != IMG_INIT_JPG) 
	{
		throw std::runtime_error(std::string("Failed to load support for jpg's: ") + SDL_GetError());
	}

	if (TTF_Init() != 0) 
	{
		throw std::runtime_error(std::string("Failed to load support for fonts: ") + SDL_GetError());
	}
}

void dae::ResourceManager::Destroy()
{
	for (auto texture : m_TextureMap)
	{
		delete texture.second;
	}
	for (auto font : m_FontMap)
	{
		delete font.second;
	}
}

dae::Texture2D* dae::ResourceManager::LoadTexture(const std::string& file)
{
	if (m_TextureMap[file]) return m_TextureMap[file];

	std::string fullPath = mDataPath + file;
	SDL_Texture* texture = IMG_LoadTexture(Renderer::GetInstance().GetSDLRenderer(), fullPath.c_str());
	if (texture == nullptr) 
	{
		throw std::runtime_error(std::string("Failed to load texture: ") + SDL_GetError());
	}
	m_TextureMap[file] = new Texture2D(texture);
	return m_TextureMap[file];
}

dae::Font* dae::ResourceManager::LoadFont(const std::string& file, unsigned int size)
{
	std::string fontName = file + std::to_string(size);
	if (m_FontMap[fontName]) return m_FontMap[fontName];

	m_FontMap[fontName] = new Font(mDataPath + file, size);
	return m_FontMap[fontName];
}
