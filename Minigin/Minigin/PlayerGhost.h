#pragma once
#include "GameObject.h"
#include "TextureComponent.h"


namespace dae
{
	class PlayerGhost : public GameObject
	{
	public:
		enum direction
		{
			NONE,
			Up,
			Down,
			Left,
			Right
		};

		enum State
		{
			none,
			Killable,
			Dead
		};
		enum Colors
		{
			Red,
			Orange,
			Pink,
			Blue
		};
		PlayerGhost(SceneManagers* pManagers);
		void MegaPelletCollected();

		void Update() override;
	private:
		TextureComponent* m_pTexComp;
		CollisionComponent* m_pCollider;
		State m_State = none;
		float m_Timer = 0.f;
		void SetTexture();
		direction m_dir;
		float speed = 130.f;

		void MoveDown();
		void MoveUp();
		void MoveLeft();
		void MoveRight();

		void Move();
	};
}
