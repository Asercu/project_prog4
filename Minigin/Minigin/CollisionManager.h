#pragma once
#include <SDL.h>
#include "Structs.h"

namespace dae
{
	class CollisionComponent;
	class GameObject;
	struct float2;

	enum ColliderTypes
	{
		Collider,
		Trigger,
		Both
	};

	struct HitInfo
	{
		float lambda;
		float2 point;
		CollisionComponent* hit = nullptr;
	};

	class CollisionManager
	{
	public:
		CollisionManager();
		~CollisionManager();
		void AddCollider(CollisionComponent* c);
		std::vector<GameObject*>* GetOverLaps(CollisionComponent* c);

		bool RayCast(float2 begin, float2 end, HitInfo& hit, ColliderTypes toHit, CollisionComponent* ignore = nullptr, std::string ignoreTag = "NOTSET");
		bool RayCast(float2 begin, float2 end, HitInfo& hit, CollisionComponent* target);

	private:
		int m_WindowHeight = 0;
		int m_WindowWidth = 0;

		std::vector<CollisionComponent*>* m_pColliders[10][10];
		std::vector<CollisionComponent*>* m_pDynamicColliders;

		//Raycast code heavily based on the code from the first year engine
		void RaycastLineCheck(std::vector<HitInfo>& hits, float2 rayBegin, float2 rayEnd, float2 linebegin, float2 lineEnd, CollisionComponent* obj);
		bool IntersectLineSegment(float2 p1, float2 p2, float2 q1, float2 q2, float& outLambda1, float& outLambda2, float epsilon = 1e-6);
		bool IsPointOnLineSegment(float2& p, float2& a, float2& b);

		bool IsOverlapping(CollisionComponent* c1, CollisionComponent* c2);
		bool IsOverlapping(SDL_Rect A, SDL_Rect B);
	};
}
