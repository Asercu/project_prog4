#pragma once
#include "GameObject.h"
#include "TextureComponent.h"

namespace dae
{
	class Pacman : public GameObject
	{
	public:
		enum direction
		{
			NONE,
			Up,
			Down,
			Left,
			Right
		};

		Pacman(SceneManagers* pManagers, int controllerID);
		int GetControllerID() { return m_ControllerID; }

	private:
		TextureComponent * pTexComp;
		int m_ControllerID = 0;
		direction m_dir = NONE;
		float speed = 150.0f;

		//gameplay functions
		void MoveDown();
		void MoveUp();
		void MoveLeft();
		void MoveRight();

		void Move();
	};
}
