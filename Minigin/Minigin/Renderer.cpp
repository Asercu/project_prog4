#include "MiniginPCH.h"
#include "Renderer.h"
#include <SDL.h>
#include "SceneManager.h"
#include "Texture2D.h"
#include "Transform.h"
#include "TextComponent.h"

void dae::Renderer::Init(SDL_Window * window)
{
	mRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (mRenderer == nullptr) 
	{
		throw std::runtime_error(std::string("SDL_CreateRenderer Error: ") + SDL_GetError());
	}
}

void dae::Renderer::Render()
{
	SDL_RenderClear(mRenderer);

	SceneManager::GetInstance().Render();
	
	SDL_RenderPresent(mRenderer);
}

void dae::Renderer::Destroy()
{
	if (mRenderer != nullptr)
	{
		SDL_DestroyRenderer(mRenderer);
		mRenderer = nullptr;
	}
}

void dae::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	SDL_QueryTexture(texture.GetSDLTexture(), nullptr, nullptr, &dst.w, &dst.h);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void dae::Renderer::RenderTexture(const Texture2D& texture, const float x, const float y, const float width, const float height) const
{
	SDL_Rect dst;
	dst.x = static_cast<int>(x);
	dst.y = static_cast<int>(y);
	dst.w = static_cast<int>(width);
	dst.h = static_cast<int>(height);
	SDL_RenderCopy(GetSDLRenderer(), texture.GetSDLTexture(), nullptr, &dst);
}

void dae::Renderer::RenderTexture(TextureComponent* pComp)
{
	SDL_Rect dest;
	Transform* pTransform = pComp->GetGameObject()->GetTransform();
	Texture2D* pTex = pComp->GetTexture();
	dest.x = static_cast<int>(pTransform->GetWorldPosition().x);
	dest.y = static_cast<int>(pTransform->GetWorldPosition().y);
	SDL_QueryTexture(pTex->GetSDLTexture(), nullptr, nullptr, &dest.w, &dest.h);
	dest.w = int(dest.w * pTransform->GetWorldScale().x);
	dest.h = int(dest.h * pTransform->GetWorldScale().y);
	SDL_Point* pPivot = nullptr;
	if (pComp->GetPivot())
	{
		pPivot = new SDL_Point();
		pPivot->x = static_cast<int>(pComp->GetPivot()->x);
		pPivot->y = static_cast<int>(pComp->GetPivot()->y);
	}
	SDL_RenderCopyEx(GetSDLRenderer(), pTex->GetSDLTexture(), nullptr, &dest, pTransform->GetWorldAngle(), pPivot, SDL_FLIP_NONE);
}

void dae::Renderer::RenderText(TextComponent* pComp)
{
	SDL_Rect dest;
	Transform* pTransform = pComp->GetGameObject()->GetTransform();
	Texture2D* pTex = pComp->GetTexture();
	dest.x = static_cast<int>(pTransform->GetWorldPosition().x);
	dest.y = static_cast<int>(pTransform->GetWorldPosition().y);
	SDL_QueryTexture(pTex->GetSDLTexture(), nullptr, nullptr, &dest.w, &dest.h);
	SDL_Point* pPivot = nullptr;
	if (pComp->GetPivot())
	{
		pPivot = new SDL_Point();
		pPivot->x = static_cast<int>(pComp->GetPivot()->x);
		pPivot->y = static_cast<int>(pComp->GetPivot()->y);
	}
	SDL_RenderCopyEx(GetSDLRenderer(), pTex->GetSDLTexture(), nullptr, &dest, pTransform->GetWorldAngle(), pPivot, SDL_FLIP_NONE);
}


