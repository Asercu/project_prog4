#pragma once
#include "Singleton.h"
#include <map>

namespace dae
{
	class Texture2D;
	class Font;
	class ResourceManager final : public Singleton<ResourceManager>
	{
		std::string mDataPath;
	public:
		ResourceManager();

		void Init(std::string&& dataPath);
		void Destroy();

		Texture2D* LoadTexture(const std::string& file);
		Font* LoadFont(const std::string& file, unsigned int size);

	private:
		std::map<std::string, Texture2D*> m_TextureMap;
		std::map<std::string, Font*> m_FontMap;
	};

}
