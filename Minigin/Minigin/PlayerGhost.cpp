#include "MiniginPCH.h"
#include "PlayerGhost.h"
#include "Transform.h"
#include "CollisionComponent.h"
#include "Time.h"
#include "PacmanScene.h"
#include "Pacman.h"

dae::PlayerGhost::PlayerGhost(SceneManagers* pManagers) : GameObject(pManagers)
{
	m_pTexComp = static_cast<TextureComponent*>(AddComponent(new TextureComponent(pManagers->pComp)));
	SetTexture();
	GetTransform()->SetScale(.25f, .25f, .25f);
	m_pCollider = static_cast<CollisionComponent*>(AddComponent(new CollisionComponent(pManagers)));
	ColliderShape CS;
	SDL_Rect box;
	box.x = 1;
	box.h = 30;
	box.w = 30;
	box.y = 1;
	CS.box = box;
	CS.dynamic = true;
	CS.type = Trigger;
	m_pCollider->SetShape(CS);

	//controller
	std::function<void(const PlayerGhost&)> pacmanMoveUP = std::bind(&PlayerGhost::MoveUp, this);
	Command<PlayerGhost>* cmdUP = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdUP);
	cmdUP->device = InputDevice::Controller;
	cmdUP->controllerID = 1;
	cmdUP->action = InputAction::Pressed;
	cmdUP->btn = ControllerButton::DPadUP;
	cmdUP->fn = pacmanMoveUP;
	pManagers->pInput->AddAction(cmdUP);

	std::function<void(const PlayerGhost&)> pacmanMoveDown = std::bind(&PlayerGhost::MoveDown, this);
	Command<PlayerGhost>* cmdDOWN = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdDOWN);
	cmdDOWN->device = InputDevice::Controller;
	cmdDOWN->controllerID = 1;
	cmdDOWN->action = InputAction::Pressed;
	cmdDOWN->btn = ControllerButton::DPadDown;
	cmdDOWN->fn = pacmanMoveDown;
	pManagers->pInput->AddAction(cmdDOWN);

	std::function<void(const PlayerGhost&)> pacmanMoveRight = std::bind(&PlayerGhost::MoveRight, this);
	Command<PlayerGhost>* cmdRIGHT = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdRIGHT);
	cmdRIGHT->device = InputDevice::Controller;
	cmdRIGHT->controllerID = 1;
	cmdRIGHT->action = InputAction::Pressed;
	cmdRIGHT->btn = ControllerButton::DPadRight;
	cmdRIGHT->fn = pacmanMoveRight;
	pManagers->pInput->AddAction(cmdRIGHT);

	std::function<void(const PlayerGhost&)> pacmanMoveLeft = std::bind(&PlayerGhost::MoveLeft, this);
	Command<PlayerGhost>* cmdLEFT = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdLEFT);
	cmdLEFT->device = InputDevice::Controller;
	cmdLEFT->controllerID = 1;
	cmdLEFT->action = InputAction::Pressed;
	cmdLEFT->btn = ControllerButton::DPadLeft;
	cmdLEFT->fn = pacmanMoveLeft;
	pManagers->pInput->AddAction(cmdLEFT);

	//Keyboard
	//std::function<void(const Pacman&)> pacmanMoveUP = std::bind(&Pacman::MoveUp, this);
	Command<PlayerGhost>* cmdUPKB = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdUPKB);
	cmdUPKB->device = InputDevice::KeyBoard;
	cmdUPKB->action = InputAction::Pressed;
	cmdUPKB->key = SDL_SCANCODE_UP;
	cmdUPKB->fn = pacmanMoveUP;
	pManagers->pInput->AddAction(cmdUPKB);

	//std::function<void(const PlayerGhost&)> pacmanMoveDown = std::bind(&PlayerGhost::MoveDown, this);
	Command<PlayerGhost>* cmdDOWNKB = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdDOWNKB);
	cmdDOWNKB->device = InputDevice::KeyBoard;
	cmdDOWNKB->action = InputAction::Pressed;
	cmdDOWNKB->key = SDL_SCANCODE_DOWN;
	cmdDOWNKB->fn = pacmanMoveDown;
	pManagers->pInput->AddAction(cmdDOWNKB);

	//std::function<void(const PlayerGhost&)> pacmanMoveRight = std::bind(&PlayerGhost::MoveRight, this);
	Command<PlayerGhost>* cmdRIGHTKB = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdRIGHTKB);
	cmdRIGHTKB->device = InputDevice::KeyBoard;
	cmdRIGHTKB->action = InputAction::Pressed;
	cmdRIGHTKB->key = SDL_SCANCODE_RIGHT;
	cmdRIGHTKB->fn = pacmanMoveRight;
	pManagers->pInput->AddAction(cmdRIGHTKB);

	//std::function<void(const PlayerGhost&)> pacmanMoveLeft = std::bind(&PlayerGhost::MoveLeft, this);
	Command<PlayerGhost>* cmdLEFTKB = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdLEFTKB);
	cmdLEFTKB->device = InputDevice::KeyBoard;
	cmdLEFTKB->action = InputAction::Pressed;
	cmdLEFTKB->key = SDL_SCANCODE_LEFT;
	cmdLEFTKB->fn = pacmanMoveLeft;
	pManagers->pInput->AddAction(cmdLEFTKB);

	std::function<void(const PlayerGhost&)> playerghostMove = std::bind(&PlayerGhost::Move, this);
	Command<PlayerGhost>* cmdMOVE = new Command<PlayerGhost>(this);
	m_Commands.push_back(cmdMOVE);
	cmdMOVE->device = InputDevice::AutoExecute;
	cmdMOVE->fn = playerghostMove;
	pManagers->pInput->AddAction(cmdMOVE);
}

void dae::PlayerGhost::MegaPelletCollected()
{
	m_State = Killable;
	m_pTexComp->SetTexture("killable.gif");
	m_Timer = 0.f;
}

void dae::PlayerGhost::Update()
{
	auto collisions = m_pCollider->GetOverLappingObjects();
	for (GameObject* pObj : *collisions)
	{
		if (pObj->GetTag() == "Pacman")
		{
			if (m_State == Killable)
			{
				static_cast<PacmanScene*>(GetParentScene())->AddScore(200, pObj);
				m_State = Dead;
				m_pTexComp->SetTexture("dead.png");
				m_Timer = 0.f;
			}
			else if (m_State == Dead)
			{
				
			}
			else
			{
				static_cast<PacmanScene*>(GetParentScene())->KillPacman(pObj);
			}
		}
	}
	delete collisions;

	m_Timer += Time::GetInstance().GetDeltaT();

	switch (m_State)
	{
	case Killable:
		if (m_Timer >= 3.f)
		{
			m_Timer = 0.f;
			m_State = none;
			SetTexture();
		}
		break;
	case Dead:
		if (m_Timer >= 3.f)
		{
			m_Timer = 0.f;
			m_State = none;
			SetTexture();
		}
		break;
	}
}

void dae::PlayerGhost::Move()
{
	switch (m_dir)
	{
	case NONE:
		break;
	case Up:
		m_Transform->Move(0.f, -speed * Time::GetInstance().GetDeltaT(), 0.f, true);
		break;
	case Down:
		m_Transform->Move(.0f, speed * Time::GetInstance().GetDeltaT(), 0.f, true);
		break;
	case Left:
		m_Transform->Move(-speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true);
		break;
	case Right:
		m_Transform->Move(speed * Time::GetInstance().GetDeltaT(), 0.f, 0.f, true);
		break;
	}
}

void dae::PlayerGhost::SetTexture()
{
	m_pTexComp->SetTexture("playerghost.png");
}

void dae::PlayerGhost::MoveRight()
{
	m_dir = Right;
}

void dae::PlayerGhost::MoveLeft()
{
	m_dir = Left;
}
void dae::PlayerGhost::MoveUp()
{
	m_dir = Up;
}
void dae::PlayerGhost::MoveDown()
{
	m_dir = Down;
}
