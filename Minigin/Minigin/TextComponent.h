#pragma once
#include "Component.h"
#include <string>
#include "Font.h"
#include "TextureComponent.h"

namespace dae
{
	class ComponentManager;
	class TextComponent : public Component
	{
	public:
		void SetText(std::string text);
		void SetFont(std::string font, unsigned int size);
		void SetColor(SDL_Color color);
		Texture2D* GetTexture() const { return m_pTexture; }
		void Update() override;
		void Render() override;
		void CleanUp() override;
		glm::vec3* GetPivot() { return m_pPivot; };
		void SetPivot(glm::vec3* pivot);
		int GetWidth();

		TextComponent(ComponentManager* pCompManager);

	private:
		std::string* m_pText;
		bool m_MustUpdate;
		Font* m_pFont;
		Texture2D* m_pTexture;
		SDL_Color m_Color;
		glm::vec3* m_pPivot;
	};
}
