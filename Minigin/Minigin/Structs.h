#pragma once


namespace dae
{
	struct float2
	{
		float x;
		float y;
		float2()
		{
			x = 0.f;
			y = 0.f;
		};
		float2(float _x, float _y)
		{
			x = _x;
			y = _y;
		};
		float2(int _x, int _y)
		{
			x = float(_x);
			y = float(_y);
		};
		float CrossProduct(float2 other)
		{
			return x * other.y - y * other.x;
		}
		float DotProduct(float2 other)
		{
			return x * other.x + y * other.y;
		}
		float getLength()
		{
			return sqrt(x * x + y * y);
		}
		float Distance(float2 p)
		{
			float2 vec{ p.x - x, p.y - y };
			return vec.getLength();
		}
		float2 Normalized()
		{
			float length{ getLength() };
			if (length < 0.0001f)
			{
				return float2{ 0, 0 };
			}
			else
			{
				return float2{ x / length, y / length };
			}
		}
	};
}

