Programming4 Exam Assignment
Arne Sercu, 2DAE5
******************************

========
=Engine=
========

Game Loop
---------

1) Time is a singleton to be easilly accessible anywhere, so the loop starts with updating the time class.
2) SceneManager.HandleInput()
	Each Scene has its own inputmanager, componentmanager and collisionmanager.
	the scenemanager will call the HandleInput() from the inputmanager of the active scene. the inputs for other scenes have no use here and this way they can be avoided.
3) SceneManager.Update()
	the scenemanager will call the update method of the active scene, efectively pausing all non active scenes.
	the scene will call the update mathods of all its gameobject children.
	the gameobjects will in their turn call the updates from their components and children
4) renderer.render
	the renderer will call the rendermethod from the active scene.

handle input
------------
the Inputmanager has two arrays of commandInterface*. this is a wrapperclass that contains a command with a reference to the object that must call the action.
the commandInterface has a command, inputdevice (controller, keyboard, or autoexecute(Gets called regardless of input, useful for AI calls)), key or button, actiontype (pressed, down or released) and some more options.
one array is for the normal commands and a second array for the threaded commands that will be executed in a seperate thread.
the handling of an commandInterface consists of executing the stored command if the the corresponding key, button,... was used since last update;
first, a thread is created for every threaded command to be handled.
then all the other commands are handled;
and lastly all the created threads are joined to have no conflicts when updating.

Update
------
the scene holds two arrays of gameobjects, m_Objects and m_ThreadedObjects.
the update uses the same priciple as the input when it comes to threading.

by doing the movement using a command, all movement is executed before the update.
in the case of the pacman implementation this means pacman and the ghosts all move before checking if they overlap and kill one another, effectively avoiding interdependencies where the checks might use outdated info.

Render
------
Even though it is a 2D engine I used 3D coordinates in the transforms. the Z value is used to define the order in which the objects need to be rendered, the higher the z value the more on top. to avoid checking the order every frame the components are sorted. the sort method is used very time an object changes its Z position.
the componentmanager has a sepparate vector with the render components (Texture and Text) to iterate fast over them.

CollisionManager
----------------
the collisionmanager contains a 2D array of vectors with all the static colliders in the scene. the grid has a fixed size of 10 * 10. There is also a vector with all the dynamic colliders.
When a new collider is added, the first thing to check is wether or not it is a dynamic collider. if it is it just gets pushed in the vector of dynamic colliders.
if not a check will be done to see in which grid spaces it occupies. for each grid space it is added to the corresponding vector.

collision methods:
GetOverlaps(Collider c): this method checks in what grid spaces collider c appears, and checks if there is an overlap between it and every collider in those gridspaces.
after that it checks all the dynamic colliders and returns a pointer to a vector with the results. mind not to forget to delete the returned vector though, as this will cause memory leaks.

Raycast: pretty straightforward, will raycast and check for hits with every collider in the same grid spaces and the dynamic colliders. there are a few optional parameters to narrow the result. Collidertypes ToHit is a required parameter to specify if the checks must include colliders, triggers or both. collider ignore is to specify one specific collider to ignore. mostly used to ignore self when starting the ray from within a collider. string ignoretag, GameObjects can be given a tag, every object with this tag will be ignored.

SceneGraph
----------
translates and scales children with parents.
Rotation is not implemented, the angles are added, but the children will only rotate around their own pivot, not the parents. I had to drop this because of time concerns.

Pacman Game
-----------
The Game Supports single player, coop and competitive multiplayer.
The gamemode is decided by the level that is loaded from a file.
the current window uses a tiled layout for the level, with 20 tiles widht en 15 height.
the legend for the file is as follows:
X: Wall
C: pellet
M: MegaPellet
O: orange ghost
P: pink ghost
B: Blue Ghost
R: Red ghost
1: player one start
2: Player two start as ms pacman (coop)
3: player two start as ghost (competitive)
' ': enmpty tile

=====
=Git=
=====
https://bitbucket.org/Asercu/project_prog4